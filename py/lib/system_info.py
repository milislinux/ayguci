import gi, os, sys, subprocess
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject
from lib import ayguci, pam

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_):
		Gtk.Window.__init__(self)
		self._ = _
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		self.name = _[3]
		self.title = _[3]
		
		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("dialog-information",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		main_box = Gtk.VBox()
		self.add(main_box)

		self.system_icon = Gtk.Image.new_from_file(os.path.dirname(sys.argv[0]) + "/icons/milis-icon.svg")
		main_box.pack_start(self.system_icon,0,1,5)

		self.os_linux_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.os_linux_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.os_version_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.os_version_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.os_release_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.os_release_label,0,0,5)
		main_box.pack_start(box,0,1,5)
		
		self.os_codename_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.os_codename_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.os_kernel_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.os_kernel_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.product_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.product_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.serial_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.serial_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.uuid_label =Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.uuid_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.board_label =Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.board_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.os_hostname_label =Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.os_hostname_label,0,0,5)

		self.edit_host_button = Gtk.Button()
		self.edit_host_button.connect("clicked",self.edit_host)
		img = Gtk.Image.new_from_stock(Gtk.STOCK_EXECUTE,36)
		self.edit_host_button.set_image(img)
		box.pack_start(self.edit_host_button,0,0,5)

		main_box.pack_start(box,0,1,5)

		sep = Gtk.VSeparator()
		main_box.pack_start(sep,0,1,5)

		self.ram_icon = Gtk.Image.new_from_file(os.path.dirname(sys.argv[0]) + "/icons/ram.svg")
		main_box.pack_start(self.ram_icon,0,1,5)

		self.ram_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.ram_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		sep = Gtk.VSeparator()
		main_box.pack_start(sep,0,1,5)

		self.cpu_icon = Gtk.Image.new_from_file(os.path.dirname(sys.argv[0]) + "/icons/cpu.svg")
		main_box.pack_start(self.cpu_icon,0,1,5)

		self.cpu_model_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.cpu_model_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.cpu_core_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.cpu_core_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		sep = Gtk.VSeparator()
		main_box.pack_start(sep,0,1,5)

		self.s_card_icon = Gtk.Image.new_from_file(os.path.dirname(sys.argv[0]) + "/icons/s_card.svg")
		main_box.pack_start(self.s_card_icon,0,1,5)

		self.video_vga_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.video_vga_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.video_3d_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.video_3d_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		sep = Gtk.VSeparator()
		main_box.pack_start(sep,0,1,5)

		self.bios_icon = Gtk.Image.new_from_file(os.path.dirname(sys.argv[0]) + "/icons/bios.svg")
		main_box.pack_start(self.bios_icon,0,1,5)

		self.bios_vendor_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.bios_vendor_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.bios_version_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.bios_version_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.bios_date_label = Gtk.Label()
		box = Gtk.HBox()
		box.pack_start(self.bios_date_label,0,0,5)
		main_box.pack_start(box,0,1,5)

		self.update_ui()

	def edit_host(self,widget):
		p = pam.pam()
		passwd =  p.auth_gui()
		if passwd: 
			dw = Gtk.MessageDialog(None,
							Gtk.DialogFlags.MODAL | Gtk.DialogFlags.DESTROY_WITH_PARENT,
							Gtk.MessageType.QUESTION,
							Gtk.ButtonsType.OK_CANCEL,
							self._[47])

			dw.set_title(self._[100])

			dialogBox = dw.get_content_area()
			userEntry = Gtk.Entry()
			#userEntry.set_visibility(False)
			userEntry.set_size_request(250,0)
			dialogBox.pack_end(userEntry, False, False, 0)

			dw.show_all()
			response = dw.run()
			text = userEntry.get_text() 
			dw.destroy()
			if (response == Gtk.ResponseType.OK) and (text != ''):
				ayguci.set_hostname(text,passwd)
				self.update_ui()
		else:
			print("Yanlış Şifre")
	def update_ui(self):
		os_data = ayguci.get("os")
		os_version = os_data["Release"]
		os_release = os_data["milis_surum"]
		os_codename = os_data["Codename"]
		os_hostname = os_data["hostname"]
		os_kernel = os_data["kernel_release"]
		
		hw_data = ayguci.get("hardware")
		hw_product = "?"
		if "vendor" in hw_data and "product" in hw_data:
			hw_product = hw_data["vendor"] +"/"+ hw_data["product"]
		hw_serial = "?"
		if "serial" in hw_data:
			hw_serial = hw_data["serial"]
		hw_uuid = "?"
		if "configuration" in hw_data and "uuid" in hw_data["configuration"]: 
			hw_uuid = hw_data["configuration"]["uuid"]
		hw_board = "?"
		if "children" in hw_data and len(hw_data["children"]) > 0 and "product" in hw_data["children"][0]: 
			hw_board = hw_data["children"][0]["product"]
		
		ram_data = ayguci.get("memory")
		total_memory = str(ram_data["total"])
		cpu_data = ayguci.get("cpu")
		cpu_model = cpu_data["product"]
		cpu_cores = cpu_data["configuration"]["cores"]
		
		gpu_data = ayguci.get("screen/card")
		gpu_info = ""
		for gpu in gpu_data:
			gpu_info += gpu["vendor"]+"/"+gpu["product"] +"\n"
		
		bios_vendor = "?"
		bios_version = "?"
		bios_date = "?"
		
		if "children" in hw_data and len(hw_data["children"]) > 0 and "children" in hw_data["children"][0]: 
			bios_data = hw_data["children"][0]["children"][0]
			if "description" in bios_data and bios_data["description"] == "BIOS" :
				bios_vendor = bios_data["vendor"]
				bios_version = bios_data["version"]
				bios_date = bios_data["date"]
		try:
			# OS
			self.os_version_label.set_text(self._[23]+os_version)
			self.os_release_label.set_text(self._[24]+os_release)
			self.os_codename_label.set_text(self._[134]+os_codename)
			self.os_kernel_label.set_text(self._[25]+os_kernel)
			# HW
			self.product_label.set_text(self._[4]+hw_product)
			self.serial_label.set_text(self._[5]+hw_serial)
			self.uuid_label.set_text(self._[6]+hw_uuid)
			self.board_label.set_text(self._[7]+hw_board)
			self.os_hostname_label.set_text(self._[8]+os_hostname)
			# MEM
			self.ram_label.set_text(self._[9]+total_memory)
			# CPU
			self.cpu_model_label.set_text(self._[10]+cpu_model)
			self.cpu_core_label.set_text(self._[11]+cpu_cores)
			# GPU
			self.video_vga_label.set_text(self._[12]+gpu_info)
			#self.video_3d_label.set_text(self._[13]+info['video']['3D'])
			# BIOS
			self.bios_vendor_label.set_text(self._[14]+bios_vendor)
			self.bios_version_label.set_text(self._[15]+bios_version)
			self.bios_date_label.set_text(self._[16]+bios_date)
		except:
			pass
