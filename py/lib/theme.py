import gi, os, subprocess
from gi.repository import Gtk, Gdk, GdkPixbuf, GLib

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_):
		Gtk.Window.__init__(self)
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		main_box = Gtk.VBox()
		self.add(main_box)

		self.name = _[57]
		self.title = _[57]

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("xfce-theme-manager",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None


		self.icon_themes = self.get_theme_names(["/usr/share/icons","~/.local/share/icons"])
		self.icon_themes.sort()
		self.gtk_themes = self.get_theme_names(["/usr/share/themes","~/.local/share/themes"])
		self.gtk_themes.sort()
		self.cursor_themes = self.get_cursors(["/usr/share/icons","~/.local/share/icons"],"cursors")
		self.icon_themes = [i for i in self.icon_themes if i not in self.cursor_themes]
		self.gtk_toolbar_styles = ["both", "both-horiz", "icons", "text"]
		self.gtk_toolbar_icon_size = ["small", "large"]
		self.hints = ["slight", "none"]
		self.font_antialiasing = ["rgba" ,"none", "grayscale"]

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		self.icon_theme_label = Gtk.Label()
		c_box.pack_start(self.icon_theme_label,False,False,5)
		self.icon_theme_combo = Gtk.ComboBoxText()
		c_box.pack_start(self.icon_theme_combo,True,True,5)
		self.combo_append(self.icon_theme_combo,self.icon_themes)

		self.gtk_theme_label = Gtk.Label()
		c_box.pack_start(self.gtk_theme_label,False,False,5)
		self.gtk_theme_combo = Gtk.ComboBoxText()
		self.gtk_theme_combo.connect("changed",self.gtk_theme_change)
		c_box.pack_start(self.gtk_theme_combo,True,True,5)
		self.combo_append(self.gtk_theme_combo,self.gtk_themes)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		self.hints_label = Gtk.Label()
		c_box.pack_start(self.hints_label,False,False,5)
		self.hints_combo = Gtk.ComboBoxText()
		c_box.pack_start(self.hints_combo,True,True,5)
		self.combo_append(self.hints_combo,self.hints)

		self.cursor_theme_label = Gtk.Label()
		c_box.pack_start(self.cursor_theme_label,False,False,5)
		self.cursor_theme_combo = Gtk.ComboBoxText()
		c_box.pack_start(self.cursor_theme_combo,True,True,5)
		self.combo_append(self.cursor_theme_combo,self.cursor_themes)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		self.tool_bar_style_lb = Gtk.Label()
		c_box.pack_start(self.tool_bar_style_lb,False,False,5)
		self.tool_bar_style_cb = Gtk.ComboBoxText()
		c_box.pack_start(self.tool_bar_style_cb,True,True,5)
		self.combo_append(self.tool_bar_style_cb,self.gtk_toolbar_styles)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		self.font_label = Gtk.Label()
		c_box.pack_start(self.font_label,False,False,5)
		self.font_button = Gtk.FontButton()
		c_box.pack_start(self.font_button,True,True,5)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		self.tool_bar_icon_s_lb = Gtk.Label()
		c_box.pack_start(self.tool_bar_icon_s_lb,False,False,5)
		self.tool_bar_icon_s_cb = Gtk.ComboBoxText()
		c_box.pack_start(self.tool_bar_icon_s_cb,True,True,5)
		self.combo_append(self.tool_bar_icon_s_cb,self.gtk_toolbar_icon_size)


		ad = Gtk.Adjustment(0, 0, 300, 1, 0, 0)
		#Focus Light
		self.cursor_theme_size_c = Gtk.Label()
		c_box.pack_start(self.cursor_theme_size_c,False,False,5)
		self.cursor_theme_size_e = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.cursor_theme_size_e.set_wrap(True)
		c_box.pack_start(self.cursor_theme_size_e,True,True,5)


		c_box = Gtk.HBox()
		self.enable_event_s_lb = Gtk.Label()
		c_box.pack_start(self.enable_event_s_lb,False,False,5)
		self.enable_event_s_cb = Gtk.CheckButton()
		c_box.pack_start(self.enable_event_s_cb,True,True,5)

		main_box.pack_start(c_box,False,False,5)
		self.e_input_feedback_s_lb = Gtk.Label()
		c_box.pack_start(self.e_input_feedback_s_lb,False,False,5)
		self.e_input_feedback_s_cb = Gtk.CheckButton()
		c_box.pack_start(self.e_input_feedback_s_cb,True,True,5)


		self.xft_antialias_lb = Gtk.Label()
		c_box.pack_start(self.xft_antialias_lb,False,False,5)
		self.xft_antialias_cb = Gtk.ComboBoxText()
		self.combo_append(self.xft_antialias_cb,self.font_antialiasing)
		c_box.pack_start(self.xft_antialias_cb,True,True,5)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		self.apply_button = Gtk.Button()
		self.apply_button.connect("clicked",self.write_config_file)
		c_box.pack_start(self.apply_button,True,True,5)


		self.read_settings_file()
		self.labels_set_texts(_)


	def gtk_theme_change(self,widget):
		theme = widget.get_active_text()
		css = Gtk.CssProvider()
		css = css.get_named(theme,None)

		Gtk.StyleContext.add_provider_for_screen(
			Gdk.Screen.get_default(),
			css,
			Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
	    )


	def write_config_file(self,widget):
		theme_name = self.gtk_theme_combo.get_active_text()
		self.run_process("interface","gtk-theme",theme_name)
		icon_theme = self.icon_theme_combo.get_active_text()
		self.run_process("interface","icon-theme",icon_theme)
		cursor_theme = self.cursor_theme_combo.get_active_text()
		self.run_process("interface","cursor-theme",cursor_theme)
		cursor_size = self.cursor_theme_size_e.get_text()
		self.run_process("interface","cursor-size",cursor_size, False)

		wf_shell_dir = os.path.join(os.path.expanduser("~/.config/wayfire.ini"))
		if os.path.exists(wf_shell_dir):
			self.settings = GLib.KeyFile()
			self.settings.load_from_file(wf_shell_dir, GLib.KeyFileFlags.NONE)
			self.settings.set_string("input","cursor_theme",cursor_theme)
			self.settings.set_string("input","cursor_size",cursor_size)
			self.settings.save_to_file(os.path.expanduser("~/.config/wayfire.ini"))

		font_name = self.font_button.get_font()
		self.run_process("interface","font-name",font_name)
		toolbar_style = self.tool_bar_style_cb.get_active_text()
		self.run_process("interface","toolbar-style",toolbar_style)
		toolbar_icons_size = self.tool_bar_icon_s_cb.get_active_text()
		self.run_process("interface","toolbar-icons-size",toolbar_icons_size)
		event_sounds = int(self.enable_event_s_cb.get_active())
		if event_sounds:
			a = "true"
		else:
			a = "false"
		self.run_process("sound","event-sounds",a,False)
		input_feedback_sounds = int(self.e_input_feedback_s_cb.get_active())
		if input_feedback_sounds:
			a = "true"
		else:
			a = "false"
		self.run_process("sound","input-feedback-sounds",a,False)
		font_hinting = self.hints_combo.get_active_text()
		self.run_process("interface","font-hinting",font_hinting)
		font_antialiasing = self.xft_antialias_cb.get_active_text()
		self.run_process("interface","font-antialiasing",font_antialiasing)

	def run_process(self, g_key,g_name,g_parement,t_nak = True):
		if t_nak:
			get = subprocess.Popen(["sh","-c","gsettings set org.gnome.desktop.{} {} '{}'".format(g_key,g_name,g_parement)], stdout = subprocess.PIPE)
		else:
			get = subprocess.Popen(["sh","-c","gsettings set org.gnome.desktop.{} {} {}".format(g_key,g_name,g_parement)], stdout = subprocess.PIPE)

	def get_prosess(self, command):
		get = subprocess.Popen(["sh","-c",command], stdout = subprocess.PIPE)
		get.wait()
		get = get.communicate()[0]
		get = get.decode("utf-8","strict")
		if get == "true":
			return 1
		elif get == "false":
			return 0
		return get[:-1].replace("'","")


	def read_settings_file(self):
		theme_name = self.get_prosess("gsettings get org.gnome.desktop.interface gtk-theme")
		icon_theme = self.get_prosess("gsettings get org.gnome.desktop.interface icon-theme")
		cursor_theme = self.get_prosess("gsettings get org.gnome.desktop.interface cursor-theme")
		cursor_size = self.get_prosess("gsettings get org.gnome.desktop.interface cursor-size")
		font_name = self.get_prosess("gsettings get org.gnome.desktop.interface font-name")
		toolbar_style = self.get_prosess("gsettings get org.gnome.desktop.interface toolbar-style")
		toolbar_icons_size = self.get_prosess("gsettings get org.gnome.desktop.interface toolbar-icons-size")
		event_sounds = self.get_prosess("gsettings get org.gnome.desktop.sound event-sounds")
		input_feedback_sounds = self.get_prosess("gsettings get org.gnome.desktop.sound input-feedback-sounds")
		font_hinting = self.get_prosess("gsettings get org.gnome.desktop.interface font-hinting")
		font_antialiasing = self.get_prosess("gsettings get org.gnome.desktop.interface font-antialiasing")

		c = self.gtk_themes.index(theme_name)
		self.gtk_theme_combo.set_active(c)
		c = self.icon_themes.index(icon_theme)
		self.icon_theme_combo.set_active(c)
		self.font_button.set_font_name(font_name)
		c = self.cursor_themes.index(cursor_theme)
		self.active_cursor_theme = self.cursor_themes[c]
		self.cursor_theme_combo.set_active(c)
		self.cursor_theme_size_e.set_value(int(cursor_size))
		c = self.gtk_toolbar_styles.index(toolbar_style)
		self.tool_bar_style_cb.set_active(c)
		c = self.gtk_toolbar_icon_size.index(toolbar_icons_size)
		self.tool_bar_icon_s_cb.set_active(c)
		c = self.hints.index(font_hinting)
		self.hints_combo.set_active(c)
		if event_sounds == "true":
			self.enable_event_s_cb.set_active(True)
		else:
			self.enable_event_s_cb.set_active(False)
		if input_feedback_sounds == "true":
			self.e_input_feedback_s_cb.set_active(True)
		else:
			self.e_input_feedback_s_cb.set_active(False)
		c = self.font_antialiasing.index(font_antialiasing)
		self.xft_antialias_cb.set_active(c)


	def split_line(self, line):
		line = line.split("=")
		if len(line) == 2:
			a = line[0].replace(" ","")
			b = line[1]
			if b[0] == " ":
				b = b[1:]
			return (a,b)
		else:
			return False


	def labels_set_texts(self,_):
		self.icon_theme_label.set_text(_[58])
		self.gtk_theme_label.set_text(_[59])
		self.cursor_theme_label.set_text(_[60])
		self.tool_bar_style_lb.set_text(_[61])
		self.tool_bar_icon_s_lb.set_text(_[62])
		self.hints_label.set_text(_[63])
		self.cursor_theme_size_c.set_text(_[64])
		self.enable_event_s_lb.set_text(_[67])
		self.e_input_feedback_s_lb.set_text(_[68])
		self.xft_antialias_lb.set_text(_[69])
		self.apply_button.set_label(_[20])
		self.font_label.set_label(_[71])


	def combo_append(self,combo,list_):
		combo.remove_all()
		for item in list_:
			combo.append_text(item)


	def get_cursors(self,dirs,type_):
		r_list = []
		for dir_ in dirs:
			dir_ = os.path.expanduser(dir_)
			if os.path.exists(dir_):
				icons = os.listdir(dir_)
				for icon in icons:
					index = os.path.join(dir_,icon,type_)
					if os.path.exists(index):
						r_list.append(icon)
		return r_list


	def get_theme_names(self,dirs):
		r_list = []
		for dir_ in dirs:
			dir_ = os.path.expanduser(dir_)
			if os.path.exists(dir_):
				icons = os.listdir(dir_)
				for icon in icons:
					index = os.path.join(dir_,icon,"index.theme")
					if os.path.exists(index):
						name = self.get_read_index_file(index)
						if name != "None":
							r_list.append(icon)
		return r_list



	def get_read_index_file(self,file_):
		f = open(file_,"r")
		reads = f.read()
		f.close()
		for coloum in reads.split():
			c = coloum.split("=")
			if c[0] == "Name":
				name = c[1]
				return name
		return "None"
