import gi, os, subprocess
from gi.repository import Gtk, GLib, GdkPixbuf, Gio, Gdk


class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_):
		Gtk.ScrolledWindow.__init__(self)
		self.parent = parent

		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		self.name = _[130]
		self.title = _[130]

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("app-launcher",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		h_box = Gtk.HBox()
		main_box = Gtk.VBox()
		h_box.pack_start(main_box,True,True,10)
		self.add(h_box)

		self.search_entry = Gtk.SearchEntry()
		self.search_entry.connect("search-changed",self.change_search_entry)
		main_box.pack_start(self.search_entry,0,1,5)

		self.mime_store = Gtk.ListStore(GdkPixbuf.Pixbuf,str,GdkPixbuf.Pixbuf,str)
		self.mime_iw = Gtk.TreeView(model=self.mime_store)
		#self.mime_iw.set_activate_on_single_click(True)
		self.mime_iw.connect("button-press-event",self.mime_iw_click,_)

		renderer = Gtk.CellRendererPixbuf()
		coloumn = Gtk.TreeViewColumn(_[1],renderer, gicon = 0)
		self.mime_iw.append_column(coloumn)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[130],renderer, text = 1)
		self.mime_iw.append_column(coloumn)
		renderer = Gtk.CellRendererPixbuf()
		coloumn = Gtk.TreeViewColumn(_[1],renderer, gicon = 2)
		self.mime_iw.append_column(coloumn)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[131],renderer, text = 3)
		self.mime_iw.append_column(coloumn)


		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.mime_iw)

		self.get_all_mimes()
		main_box.pack_start(scroll,1,1,5)

	def change_search_entry(self,widget):
		text = widget.get_text().lower()
		self.mime_iw_set_items(text)


	def get_mime_infos(self,dir_,key):
		if os.path.exists(dir_):
			r_mimes = {}
			settings = GLib.KeyFile()
			settings.load_from_file(dir_, GLib.KeyFileFlags.NONE)
			mimes = settings.get_keys(key)
			mimes = mimes[0]
			for mime in mimes:
				desktops = settings.get_string_list(key,mime)
				r_mimes[mime] = desktops
			return r_mimes, settings
		return False

	def get_icon(self,icon_names):
		for icon_name in icon_names:
			try:
				pixbuf = self.d_icon_theme.load_icon(icon_name, 32, 0)
				return pixbuf
			except:
				continue
		return None

	def get_all_mimes(self):
		dir_ = os.path.expanduser("~/.config/mimeapps.list")
		self.user_mimes, self.user_settings = self.get_mime_infos(dir_,"Default Applications")
		dir_ = "/usr/share/applications/mimeinfo.cache"
		self.all_mimes, all_settings = self.get_mime_infos(dir_,"MIME Cache")
		if self.all_mimes:
			self.mime_iw_set_items(self.search_entry.get_text())

	def mime_iw_set_items(self,text=""):
		self.mime_store.clear()
		u_mime_keys = self.user_mimes.keys()
		for key in self.all_mimes.keys():
			if text in key:
				try:
					icon = self.d_icon_theme.load_icon(key.replace("/","-"),32,Gtk.IconLookupFlags.FORCE_REGULAR)
				except:
					icon = self.icon
				if key in u_mime_keys:
					u_app_name = self.user_mimes[key][0]
				else:
					u_app_name = self.all_mimes[key][0]
				try:
					app = Gio.DesktopAppInfo.new(u_app_name)
				except:
					app = None
				try:
					app_icon = self.get_icon(app.get_icon().get_names())
				except:
					app_icon = None
				try:
					u_app_name = app.get_name()
				except:
					u_app_name = None
				self.mime_store.append([icon,key,app_icon,u_app_name])

	def back_up_module(self, widget, mime_type):
		self.user_settings.remove_key("Default Applications",mime_type)
		dir_ = os.path.expanduser("~/.config/mimeapps.list")
		self.user_settings.save_to_file(dir_)
		self.get_all_mimes()

	def mime_iw_click(self,widget,event,_):
		select = self.mime_iw.get_selection()
		tree_model, tree_iter = select.get_selected()
		if tree_iter != None and tree_model != None:
			select = tree_model[tree_iter]
			u_mime_keys = self.user_mimes.keys()
			if event.button == 1 and event.type == Gdk.EventType._2BUTTON_PRESS:
				dialog = Gtk.AppChooserDialog.new_for_content_type(None, Gtk.DialogFlags.DESTROY_WITH_PARENT, select[1])
				y_n = dialog.run()
				if y_n == Gtk.ResponseType.OK:
					app_info = dialog.get_app_info()
					desktop_name = app_info.get_filename()
					desktop_name = os.path.split(desktop_name)[1]
					if select[1] in u_mime_keys:
						if desktop_name in self.user_mimes[select[1]]:
							self.user_mimes[select[1]].remove(desktop_name)
							self.user_mimes[select[1]].insert(0,desktop_name)
							self.user_settings.set_string_list("Default Applications",select[1],self.user_mimes[select[1]])
						else:
							self.user_mimes[select[1]].insert(0,desktop_name)
							self.user_settings.set_string_list("Default Applications",select[1],self.user_mimes[select[1]])
					else:
						self.user_settings.set_string_list("Default Applications",select[1],[desktop_name])
					dir_ = os.path.expanduser("~/.config/mimeapps.list")
					self.user_settings.save_to_file(dir_)
					self.get_all_mimes()
				dialog.destroy()
			elif event.button == 3:
				if select[1] in u_mime_keys:
					menu = Gtk.Menu()
					menu_item = Gtk.ImageMenuItem(_[132])
					img = Gtk.Image.new_from_stock(Gtk.STOCK_REMOVE,Gtk.IconSize.MENU)
					menu_item.set_image(img)
					menu_item.connect("activate",self.back_up_module, select[1])
					menu.add(menu_item)
					menu.attach_to_widget(widget)
					menu.show_all()
					menu.popup_at_pointer()
