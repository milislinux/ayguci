import gi, os, sys, subprocess, cairo
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject
from lib import ayguci, coordinats, keyboards
import json

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_):
		Gtk.Window.__init__(self)
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		self.name = _[49]
		self.title = _[49]

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("keyboard",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		m_box = Gtk.VBox()
		self.add(m_box)

		self.desktop_settings = ayguci.get("desktop")
		self.aktif_yerel = self.desktop_settings["localization"]["language"].replace("utf-8","UTF-8")
		self.tum_yereller = ayguci.get("locale")["supports"]
		self.duzenli_yereller = keyboards.ulkeler
		for yerel in self.tum_yereller:
			y = yerel.split("_")[0]
			try:
				self.duzenli_yereller[y][1].append(yerel)
			except:
				self.duzenli_yereller["order"][1].append(yerel)

		box = Gtk.HBox()
		self.y_ulke_yazi = Gtk.Label()
		box.pack_start(self.y_ulke_yazi,0,0,5)
		self.y_ulke_combo = Gtk.ComboBoxText()
		self.y_ulke_combo.connect("changed", self.y_ulke_combo_secildi)

		box.pack_start(self.y_ulke_combo,1,1,5)
		self.yerel_yazi = Gtk.Label()
		box.pack_start(self.yerel_yazi,0,0,5)
		self.yerel_combo = Gtk.ComboBoxText()
		box.pack_start(self.yerel_combo,1,1,5)
		self.y_degistir_dugme = Gtk.Button()
		self.y_degistir_dugme.connect("clicked",self.y_degistir_dugme_tiklandi)
		box.pack_start(self.y_degistir_dugme,0,0,5)
		m_box.pack_start(box,0,0,5)

		sayac = 0
		for v in self.duzenli_yereller.values():
			self.y_ulke_combo.append_text(v[0])
			for yerel in v[1]:
				if self.aktif_yerel in yerel:
					self.y_ulke_combo.set_active(sayac)
			sayac += 1

		self.resim = Gtk.DrawingArea()
		self.resim.set_size_request (630, 200)
		self.resim.connect("draw", self.expose)
		m_box.pack_start(self.resim,0,0,5)

		box = Gtk.HBox()
		self.model_yazi = Gtk.Label()
		box.pack_start(self.model_yazi,0,0,5)
		self.model_combo = Gtk.ComboBoxText()
		box.pack_start(self.model_combo,1,1,5)
		m_box.pack_start(box,0,0,5)

		model = list(keyboards.modeller.keys())
		model.sort()

		self.is_wf_ini_get()

		sayac = 0
		for mod_ in model:
			self.model_combo.append_text(keyboards.modeller[mod_])
			if mod_ == self.key_model:
				self.model_combo.set_active(sayac)
			sayac += 1

		box = Gtk.HBox()
		self.ulke_yazi = Gtk.Label()
		box.pack_start(self.ulke_yazi,0,0,5)
		self.ulke_combo = Gtk.ComboBoxText()
		box.pack_start(self.ulke_combo,1,1,5)

		sayac = 0
		for duzen in keyboards.duzenler.keys():
			self.ulke_combo.append_text(keyboards.duzenler[duzen])
			if duzen == self.key_layout:
				self.ulke_combo.set_active(sayac)
			sayac += 1

		self.tur_yazi = Gtk.Label()
		box.pack_start(self.tur_yazi,0,0,5)
		self.tur_combo = Gtk.ComboBoxText()
		box.pack_start(self.tur_combo,1,1,5)
		m_box.pack_start(box,0,0,5)

		sayac = 0
		self.tur_combo.append_text("Default")
		self.tur_combo.set_active(sayac)
		for k in keyboards.varyantlar[self.key_layout]:
			variant = list(k.values())[0]
			self.tur_combo.append_text(variant)
			if variant == self.key_variant:
				self.tur_combo.set_active(sayac)
			sayac += 1

		self.text_entry = Gtk.Entry()
		m_box.pack_start(self.text_entry,0,0,5)

		self.y_ulke_yazi.set_text(_[135])
		self.yerel_yazi.set_text(_[136])
		self.y_degistir_dugme.set_label(_[20])
		self.model_yazi.set_text(_[50])
		self.ulke_yazi.set_text(_[51])
		self.tur_yazi.set_text(_[52])

		self.model_combo.connect("changed", self.klavye_modeli_secildi)
		self.ulke_combo.connect("changed", self.ulke_secildi)
		self.tur_combo.connect("changed", self.tur_secildi)

		self.cizim_baslangic(self.key_layout,self.key_model,self.key_variant)

	def y_degistir_dugme_tiklandi(self, widget):
		p_text = self.yerel_combo.get_active_text()
		# { "add" : "hu_HU.UTF-8 UTF-8" }
		# { "del" : "hu_HU.UTF-8 UTF-8" }
		# 1- locale desteğinin eklenmesi
		req = { "add" : p_text}
		response = ayguci.post("locale", req)
		if "add" in response:
			set_locale = response["add"].split()[0]
		# 2- locale nin masa.ini set edilmesi
		req = {
			"section" : "localization",
			"key"	  : "language",
			"value"	  : set_locale,
		}
		response = ayguci.post("desktop", req)
		print("aktif dil ayarı:",response)

	def y_ulke_combo_secildi(self, widget):
		self.yerel_combo.remove_all()
		active = widget.get_active_text()
		for yerel in self.duzenli_yereller.values():
			if active == yerel[0]:
				sayac = 0
				aktif = 0
				for y in yerel[1]:
					if self.aktif_yerel in y:
						aktif = sayac
					self.yerel_combo.append_text(y)
					sayac += 1
				self.yerel_combo.set_active(aktif)
				break

	def is_wf_ini_get(self):
		# layout
		if "layout" in self.desktop_settings["keyboard"]:
			self.key_layout = self.desktop_settings["keyboard"]["layout"]
		else:
			self.key_layout = "tr"
		# model
		if "model" in self.desktop_settings["keyboard"] and self.desktop_settings["keyboard"]["model"] != "":
			self.key_model = self.desktop_settings["keyboard"]["model"]
		else:
			self.key_model = "pc105"
		# variant
		if "variant" in self.desktop_settings["keyboard"] and self.desktop_settings["keyboard"]["variant"] != "":
			self.key_variant = self.desktop_settings["keyboard"]["variant"]
		else:
			self.key_variant = ""

	def klavye_modeli_secildi(self,widget):
		model = list(keyboards.modeller.keys())
		for mod_ in model:
			if keyboards.modeller[mod_] == self.model_combo.get_active_text():
				self.key_model = mod_
				req = {
					"section" : "keyboard",
					"key"	  : "model",
					"value"	  : self.key_model,
				}
				response = ayguci.post("desktop", req)
		self.cizim_baslangic(self.key_layout,self.key_model,self.key_variant)

	def ulke_secildi(self,widget):
		for duzen in keyboards.duzenler.keys():
			if keyboards.duzenler[duzen] == self.ulke_combo.get_active_text():
				self.key_layout = duzen
				req = {
					"section" : "keyboard",
					"key"	  : "layout",
					"value"	  : self.key_layout,
				}
				response = ayguci.post("desktop", req)
		self.tur_combo.remove_all()
		self.key_variant = ""
		self.tur_combo.append_text("Default")
		try:
			print(self.key_variant)
			for k in keyboards.varyantlar[self.key_layout]:
				self.tur_combo.append_text(list(k.values())[0])
		except:
			pass
		self.tur_combo.set_active(0)

	def tur_secildi(self,widget):
		if self.tur_combo.get_active_text() == "Default":
			self.cizim_baslangic(self.key_layout,self.key_model,self.key_variant)
		else:
			try:
				for k in keyboards.varyantlar[self.key_layout]:
					if list(k.values())[0] == self.tur_combo.get_active_text():
						self.key_variant = list(k.keys())[0]
						req = {
							"section" : "keyboard",
							"key"	  : "variant",
							"value"	  : self.key_variant,
						}
						response = ayguci.post("desktop", req)
						self.cizim_baslangic(self.key_layout,self.key_model,self.key_variant)
			except:
				pass


	def duzenler_doldur(self):
		self.ulke_combo.remove_all()
		keys = list(keyboards.duzenler.keys())
		keys.sort()
		for key_ in keys:
			self.ulke_combo.append_text(keyboards.duzenler[key_])
		self.ulke_combo.set_active(0)

	def expose(self, widget, cr):
		klavye = cairo.ImageSurface.create_from_png(os.path.dirname(sys.argv[0]) + "/icons/klavye.png")
		cr.set_source_surface(klavye,0,0)
		cr.paint()
		koordinat_listesi = ((10, 20), (65, 65), (85, 105), (60, 150))
		sayac = 0
		if self.key_model in keyboards.klavye_modeli.keys():
			for key_list in keyboards.klavye_modeli[self.key_model]:
				coordinat = koordinat_listesi[sayac]
				sayac += 1
				for num, key in enumerate(key_list):
					try:
						cr.set_source_rgb(204, 255, 0)
						cr.select_font_face("Noto Sans", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
						cr.set_font_size(10)
						cr.move_to(coordinat[0]+(42*num), coordinat[1])
						cr.show_text(self.tus_yerlesimi[key][1])
						cr.set_source_rgb(255, 255, 255)
						cr.set_font_size(12)
						cr.move_to(coordinat[0]+10+(42*num), coordinat[1]+15)
						cr.show_text(self.tus_yerlesimi[key][0])
					except KeyError as err:
						print(key)

	def cizim_baslangic(self, layout, model, variant):
		if model in keyboards.klavye_modeli.keys():
			self.tus_yerlesimi = self.unicodeToString(model, layout, variant)
			if self.tus_yerlesimi:
				self.resim.queue_draw()

	def unicodeToString(self, model, layout, variant=""):
		try:
			keycodes = {}
			tus_yerlesimi_command = subprocess.Popen([os.path.dirname(sys.argv[0]) + "/sh/ckbcomp", "-model", model, "-layout", layout, "-variant", variant],
												stdout=subprocess.PIPE)
			ciktilar = tus_yerlesimi_command.stdout.read()
			for cikti in ciktilar.decode("utf-8").split("\n"):
				if cikti.startswith("keycode") and cikti.count("="):
					cikti = cikti.split()
					if cikti[3].startswith("U+") or cikti[3].startswith("+U"):
						first = bytes("\\u" + cikti[3][2:].replace("+", ""), "ascii").decode("unicode-escape")
						second = bytes("\\u" + cikti[4][2:].replace("+", ""), "ascii").decode("unicode-escape")
						keycodes[int(cikti[1])] = [first, second]
			return keycodes
		except:
			return False

