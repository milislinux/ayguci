import gi, os, subprocess, configparser
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject, Gio, GLib

class AddEditCommand(Gtk.Popover):
	def __init__(self,parent,_,is_e_input,command):
		super(AddEditCommand,self).__init__()
		self.m_box = Gtk.VBox()
		self._ = _
		self.parent = parent
		self.is_e_input = is_e_input
		self.add(self.m_box)
		box = Gtk.HBox()
		self.m_box.pack_start(box,0,0,5)

		self.c_label = Gtk.Label()
		self.c_label.set_text(_[104])
		box.pack_start(self.c_label,0,0,5)

		self.c_combo = Gtk.ComboBoxText()
		self.c_combo.connect("changed",self.cmmnd_changed)
		box.pack_start(self.c_combo,1,1,5)

		if is_e_input == 0 or is_e_input == 1:
			box = Gtk.HBox()
			self.m_box.pack_start(box,0,0,5)
			self.e_i_lbl = Gtk.Label()
			if is_e_input == 0:
				self.e_i_lbl.set_label(_[142])
			else:
				self.e_i_lbl.set_label(_[144])
			box.pack_start(self.e_i_lbl,0,0,5)
			self.e_input = Gtk.Entry()
			box.pack_start(self.e_input,1,1,5)
			if command != None:
				self.e_input.set_text(command.split("_")[1])

		self.ni_box = Gtk.HBox()
		self.ni_box.set_no_show_all(True)
		self.m_box.pack_start(self.ni_box,0,0,5)
		self.ni_lbl = Gtk.Label()
		self.ni_lbl.set_text(_[154])
		self.ni_box.pack_start(self.ni_lbl,0,0,5)
		self.notfy_icon = Gtk.Entry()
		self.ni_box.pack_start(self.notfy_icon,1,1,5)

		self.nc_box = Gtk.HBox()
		self.nc_box.set_no_show_all(True)
		self.m_box.pack_start(self.nc_box,0,0,5)
		self.nc_lbl = Gtk.Label()
		self.nc_lbl.set_text(_[155])
		self.nc_box.pack_start(self.nc_lbl,0,0,5)
		self.notfy_text = Gtk.Entry()
		self.nc_box.pack_start(self.notfy_text,1,1,5)

		self.cmnd_box = Gtk.HBox()
		self.cmnd_box.set_no_show_all(True)
		self.m_box.pack_start(self.cmnd_box,0,0,5)
		self.cmnd_lbl = Gtk.Label()
		self.cmnd_lbl.set_text(_[157])
		self.cmnd_box.pack_start(self.cmnd_lbl,0,0,5)
		self.cmnd_text = Gtk.Entry()
		self.cmnd_box.pack_start(self.cmnd_text,1,1,5)

		apply_btn = Gtk.Button()
		self.m_box.pack_start(apply_btn,0,0,5)

		if command != None:
			remove_btn = Gtk.Button()
			remove_btn.connect("clicked",self.remove_clicked,command)
			remove_btn.set_label(_[110])
			self.m_box.pack_start(remove_btn,0,0,5)
			apply_btn.connect("clicked",self.edit_clicked,command)
			apply_btn.set_label(_[106])
		else:
			apply_btn.connect("clicked",self.apply_clicked)
			apply_btn.set_label(_[146])

		a_commands = [_[147], _[148], _[149], _[150], _[151], _[156], _[152], _[153], _[157]]
		i = 0
		active = False
		for c in a_commands:
			self.c_combo.append_text(c)
			if command != None:
				t_cm = self.parent.settings["power"][command]
				if t_cm in self.parent.cmnd_changer.keys() and self.parent.cmnd_changer[t_cm] == c:
					self.c_combo.set_active(i)
					active = True
			i += 1
		if not active and command != None:
			t_cm = self.parent.settings["power"][command]
			t = t_cm.split()
			if t[0] == "notify-send":
				self.c_combo.set_active(7)
				s = 1
				for k in t:
					if k == "-i":
						self.notfy_icon.set_text(t[s])
						self.notfy_text.set_text(" ".join(t[s+1:]))
					s += 1
			else:
				self.c_combo.set_active(8)
				self.cmnd_text.set_text(self.parent.settings["power"][command])

	def edit_clicked(self,widget,cmmnd):
		self.remove_clicked(None,cmmnd,False)
		self.apply_clicked(None)

	def remove_clicked(self,widget,cmmnd,no_edit=True):
		del self.parent.settings["power"][cmmnd]
		if no_edit:
			self.parent.set_power()
			self.parent.get_power()
			self.destroy()

	def apply_clicked(self, widget):
		cmnd_changer = {self._[150]:"guc -lock",self._[149]:"guc -poweroff",self._[148]:"guc -reboot",
				self._[147]:"guc -suspend",self._[151]:"guc -off screen",
				self._[156]:"guc -tog screen",self._[152]:"guc -off wifi"}
		cmnd = self.c_combo.get_active_text()
		if self._[153] == cmnd:
			ni = self.notfy_icon.get_text()
			nc = self.notfy_text.get_text()
			if ni != "" and nc != "":
				cmnd = "notify-send -i {} {}".format(ni,nc)
			else:
				print("ALANLAR BOŞ")
				return
		elif self._[157] == cmnd:
			ct = self.cmnd_text.get_text()
			if ct != "":
				cmnd = ct
			else:
				print("ALANLAR BOŞ")
				return
		elif cmnd in cmnd_changer.keys():
			cmnd = cmnd_changer[cmnd]
		if self.is_e_input == 0:
			self.parent.settings["power"]["idle_{}".format(self.e_input.get_text())] = cmnd
		elif self.is_e_input == 1:
			self.parent.settings["power"]["bat_{}".format(self.e_input.get_text())] = cmnd
		elif self.is_e_input == 2:
			self.parent.settings["power"]["sleep"] = cmnd
		elif self.is_e_input == 3:
			self.parent.settings["power"]["lid_close"] = cmnd
		self.parent.set_power()
		self.parent.get_power()
		self.destroy()

	def cmmnd_changed(self,widget):
		self.ni_box.hide()
		self.nc_box.hide()
		self.cmnd_box.hide()
		if self._[153] == widget.get_active_text():
			self.ni_box.show()
			self.nc_box.show()
			self.notfy_icon.show()
			self.ni_lbl.show()
			self.nc_lbl.show()
			self.notfy_icon.show()
			self.notfy_text.show()
		elif self._[157] == widget.get_active_text():
			self.cmnd_box.show()
			self.cmnd_lbl.show()
			self.cmnd_text.show()

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_):
		Gtk.Window.__init__(self)
		self._ = _
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		self.name = _[137]
		self.title = _[137]
		self.cmnd_changer = {"guc -lock":self._[150],"guc -poweroff":self._[149],"guc -reboot":self._[148],
				"guc -suspend":self._[147],"guc -off screen":self._[151],
				"guc -tog screen":self._[156],"guc -off wifi":self._[152]}

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("gnome-power-manager",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		main_box = Gtk.VBox()
		self.add(main_box)

		############################ IDLE ##############################
		idle_label = Gtk.Label(_[138])
		box = Gtk.HBox()
		box.pack_start(idle_label,0,0,5)
		main_box.pack_start(box,0,0,5)

		idle_button = Gtk.Button()
		idle_button.connect("clicked",self.popever_open, 0)
		idle_button.set_label(_[145])
		box.pack_start(idle_button,0,0,5)

		self.idle_store = Gtk.ListStore(str,str,str)
		self.idle_iw = Gtk.TreeView(model=self.idle_store)
		self.idle_iw.connect("row-activated",self.iw_activate,0)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[142],renderer, text = 0)
		self.idle_iw.append_column(coloumn)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[143],renderer, text = 1)
		self.idle_iw.append_column(coloumn)
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.idle_iw)
		box = Gtk.HBox()
		box.pack_start(scroll,1,1,5)
		main_box.pack_start(box,1,1,5)
		########################### BATTERY #############################
		battery_label = Gtk.Label(_[139])
		box = Gtk.HBox()
		box.pack_start(battery_label,0,0,5)
		main_box.pack_start(box,0,0,5)

		battery_button = Gtk.Button()
		battery_button.connect("clicked",self.popever_open, 1)
		battery_button.set_label(_[145])
		box.pack_start(battery_button,0,0,5)

		self.battery_store = Gtk.ListStore(str,str,str)
		self.battery_iw = Gtk.TreeView(model=self.battery_store)
		self.battery_iw.connect("row-activated",self.iw_activate,1)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[144],renderer, text = 0)
		self.battery_iw.append_column(coloumn)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[143],renderer, text = 1)
		self.battery_iw.append_column(coloumn)
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.battery_iw)
		box = Gtk.HBox()
		box.pack_start(scroll,1,1,5)
		main_box.pack_start(box,1,1,5)
		########################### SLEEP #############################
		sleep_label = Gtk.Label(_[140])
		box = Gtk.HBox()
		box.pack_start(sleep_label,0,0,5)
		main_box.pack_start(box,0,0,5)

		sleep_button = Gtk.Button()
		sleep_button.connect("clicked",self.popever_open, 2)
		sleep_button.set_label(_[145])
		box.pack_start(sleep_button,0,0,5)

		self.sleep_store = Gtk.ListStore(str,str)
		self.sleep_iw = Gtk.TreeView(model=self.sleep_store)
		self.sleep_iw.connect("row-activated",self.iw_activate,2)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[143],renderer, text = 0)
		self.sleep_iw.append_column(coloumn)
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.sleep_iw)
		box = Gtk.HBox()
		box.pack_start(scroll,1,1,5)
		main_box.pack_start(box,1,1,5)
		######################### LİD_CLOSE ###########################
		lid_close_label = Gtk.Label(_[141])
		box = Gtk.HBox()
		box.pack_start(lid_close_label,0,0,5)
		main_box.pack_start(box,0,0,5)

		lid_close_button = Gtk.Button()
		lid_close_button.connect("clicked",self.popever_open, 3)
		lid_close_button.set_label(_[145])
		box.pack_start(lid_close_button,0,0,5)

		self.lid_close_store = Gtk.ListStore(str,str)
		self.lid_close_iw = Gtk.TreeView(model=self.lid_close_store)
		self.lid_close_iw.connect("row-activated",self.iw_activate,3)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[143],renderer, text = 0)
		self.lid_close_iw.append_column(coloumn)
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.lid_close_iw)
		box = Gtk.HBox()
		box.pack_start(scroll,1,1,5)
		main_box.pack_start(box,1,1,5)

		self.get_power()

	def iw_activate(self,widget,path,coloumn,iw_number):
		select = widget.get_selection()
		tree_model, tree_iter = select.get_selected()
		if tree_iter != None and tree_model != None:
			select = tree_model[tree_iter]
			self.popever_open(widget,iw_number,select[-1])

	def popever_open(self, widget, i, cmmnd=None):
		user_add_edit = AddEditCommand(self, self._, i, cmmnd)
		user_add_edit.set_position(Gtk.PositionType.BOTTOM)
		user_add_edit.set_relative_to(widget)
		user_add_edit.show_all()
		user_add_edit.popup()

	def item_in_cmnd_changer(self,item):
		if item in self.cmnd_changer.keys():
			return self.cmnd_changer[item]
		else:
			return item

	def set_power(self):
		self.config_file = os.path.join(os.path.expanduser("~/.config/masa.ini"))
		if os.path.exists(self.config_file):
			with open(self.config_file, 'w') as cf:
				self.settings.write(cf)
			subprocess.Popen(["guc","-update","waybar"])

	def get_power(self):
		try:
			self.config_file = os.path.join(os.path.expanduser("~/.config/masa.ini"))
			if os.path.exists(self.config_file):
				self.settings = configparser.ConfigParser()
				self.settings.read(self.config_file)
				self.sleep_store.clear()
				self.lid_close_store.clear()
				self.idle_store.clear()
				self.battery_store.clear()
				for cmnd in self.settings["power"].keys():
					if cmnd == "sleep":
						self.sleep_store.append([self.item_in_cmnd_changer(self.settings["power"][cmnd]),cmnd])
					if cmnd == "lid_close":
						self.lid_close_store.append([self.item_in_cmnd_changer(self.settings["power"][cmnd]),cmnd])
					if "idle" in cmnd:
						time = cmnd.split("_")[1]
						self.idle_store.append([cmnd,self.item_in_cmnd_changer(self.settings["power"][cmnd]),cmnd])
					if  "bat" in cmnd:
						time = cmnd.split("_")[1]
						self.battery_store.append([cmnd,self.item_in_cmnd_changer(self.settings["power"][cmnd]),cmnd])
		except:
			print("Masa.ini okunamadı!!!")


