import gi, subprocess, os
from gi.repository import Gtk
from lib import md2

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_,documents):
		Gtk.Window.__init__(self)
		self.parent = parent

		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		self.name = _[101]
		self.title = _[101]

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("document-viewer",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		h_box = Gtk.HBox()
		self.main_box = Gtk.VBox()
		h_box.pack_start(self.main_box,True,True,10)
		self.add(h_box)
		self.make_buttons(documents)

	def make_buttons(self,documents):
		documents.sort()
		for document in documents:
			doc_name = os.path.split(document)[1].replace(".md","")
			button = Gtk.Button()
			button.connect("clicked",self.run_md_file,document,doc_name)
			button.set_label(doc_name)
			self.main_box.pack_start(button,False,False,5)

	def run_md_file(self,widget,md_file,doc_name):
		try:
			f = open(md_file,"r")
			read_text = f.read()
			read_text = md2.markdown(read_text,extras=["fenced-code-blocks"])
			doc_name = "/tmp/{}.html".format(doc_name)
			f = open(doc_name,"w")
			f.write(read_text)
			f.close()
			subprocess.Popen(["firefox",doc_name], stdout = subprocess.PIPE)
		except:
			print("Hata oluştu documents: run_md_file func")
