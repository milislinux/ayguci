import gi, locale, os, sys, subprocess, cairo
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject
from lib import ayguci, coordinats, translate
from datetime import datetime

l = locale.getdefaultlocale()
l = l[0].split("_")[0]
locales = list(translate.translate.keys())
if l not in locales:
	l = "en"
_ = translate.translate[l]

class DateEdit(Gtk.Popover):
	def __init__(self,parent,clock,b_text):
		super(DateEdit,self).__init__()
		m_box = Gtk.VBox()
		self.add(m_box)
		self.parent = parent

		self.cal = Gtk.Calendar()
		m_box.pack_start(self.cal,1,1,5)

		button = Gtk.Button()
		button.connect("clicked",self.day_change)
		button.set_label(b_text)
		m_box.pack_start(button,1,1,5)

	def change_d_m_format(self,number):
		number = str(number)
		if len(number) < 2:
			return "0"+number
		else:
			return number
		
	def day_change(self,widget):
		y,m,d = self.cal.get_date()
		m = m + 1
		y = str(y)
		m = self.change_d_m_format(m)
		d = self.change_d_m_format(d)
		now = datetime.now()
		n_date = "{}/{}/{}".format(m,d,y)
		self.parent.date_label.set_text("{}/{}/{}".format(d,m,y))
		ayguci.set_datetime_manuel(n_date+" "+now.strftime("%H:%M:%S"))
		self.destroy()

class ClockEdit(Gtk.Popover):
	def __init__(self,parent,clock,b_text):
		super(ClockEdit,self).__init__()
		m_box = Gtk.VBox()
		self.add(m_box)

		self.t_e = Gtk.Entry()
		self.t_e.set_text(clock)
		m_box.pack_start(self.t_e,1,1,5)
		button = Gtk.Button()
		button.connect("clicked",self.change_clock)
		button.set_label(b_text)
		m_box.pack_start(button,1,1,5)

	def change_clock(self, widget):
		c_text = self.t_e.get_text()
		c_list = c_text.split(":")
		if len(c_list) != 3:
			return False
		for t in c_list:
			if not t.isdecimal():
				return False
		now = datetime.now()
		ayguci.set_datetime_manuel(now.strftime("%m/%d/%Y")+" "+c_text)
		self.destroy()

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_):
		Gtk.Window.__init__(self)
		self.parent = parent
		self.passwd = "test"
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		self.name = _[17]
		self.title = _[17]
		self.get_timezone()

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("time",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		main_box = Gtk.VBox()
		self.add(main_box)

		box = Gtk.HBox()
		main_box.pack_start(box,0,1,5)

		self.network_time_label = Gtk.Label()
		self.network_time_label.set_text(_[133])
		box.pack_start(self.network_time_label,0,1,5)

		self.network_time_switch = Gtk.Switch()
		box.pack_end(self.network_time_switch,0,1,5)
		self.network_time_switch.connect("notify::active", self.network_switch_change)

		box = Gtk.HBox()
		main_box.pack_start(box,0,1,5)

		self.datetime_utc_label = Gtk.Label()
		self.datetime_utc_label.set_text(_[21])
		box.pack_start(self.datetime_utc_label,0,1,5)

		self.utc_switch = Gtk.Switch()
		self.utc_switch.connect("notify::active", self.switch_change)
		box.pack_end(self.utc_switch,0,1,5)

		box = Gtk.HBox()
		main_box.pack_start(box,0,1,5)

		self.active_clock = Gtk.Label()
		self.active_clock.set_text(_[26])
		box.pack_start(self.active_clock,1,1,5)

		self.clock_label = Gtk.Label()
		self.display_clock()
		box.pack_start(self.clock_label,1,1,5)

		self.clock_edit = Gtk.Button()
		self.clock_edit.connect("clicked",self.edit_clock_pop,_)
		img = Gtk.Image.new_from_stock(Gtk.STOCK_EXECUTE,36)
		self.clock_edit.set_image(img)
		box.pack_start(self.clock_edit,0,0,5)

		self.active_date = Gtk.Label()
		self.active_date.set_text(_[27])
		box.pack_start(self.active_date,1,1,5)

		self.date_label = Gtk.Label()
		now = datetime.now()
		self.date_label.set_text(now.strftime("%d/%m/%Y"))
		box.pack_start(self.date_label,1,1,5)

		self.date_edit = Gtk.Button()
		self.date_edit.connect("clicked",self.date_edit_pop,_)
		img = Gtk.Image.new_from_stock(Gtk.STOCK_EXECUTE,36)
		self.date_edit.set_image(img)
		box.pack_start(self.date_edit,0,0,5)

		self.map = Gtk.DrawingArea()
		self.map.set_size_request(630, 315)
		self.map.connect("draw", self.expose)
		self.map.set_events(Gdk.EventMask.BUTTON_PRESS_MASK)
		self.map.connect("button_press_event", self.map_click)
		main_box.pack_start(self.map,0,1,5)

		hbox = Gtk.HBox()
		main_box.pack_start(hbox,0,1,5)
		self.zone_label = Gtk.Label()
		self.zone_label.set_text(_[18])
		hbox.pack_start(self.zone_label,1,1,5)
		self.zone_combo = Gtk.ComboBoxText()
		hbox.pack_start(self.zone_combo,1,1,5)
		self.country_label = Gtk.Label()
		self.country_label.set_text(_[19])
		hbox.pack_start(self.country_label,1,1,5)
		self.country_combo = Gtk.ComboBoxText()
		hbox.pack_start(self.country_combo,1,1,5)

		self.get_coordinats()
		self.zone_combo.connect("changed", self.zone_combo_change)
		self.country_combo.connect("changed", self.country_combo_change)
		self.zone_combo_change(None)

		self.apply_button = Gtk.Button()
		self.apply_button.connect("clicked",self.apply_button_click)
		self.apply_button.set_label(_[20])
		main_box.pack_start(self.apply_button,0,1,5)
		self.get_datetime_utc()
		self.timer = GObject.timeout_add(1000, self.display_clock)

		self.network_time_switch.set_active(self.get_service_status("chrony")[0])

	def edit_clock_pop(self,widget,_):
		clock_edit = ClockEdit(self,self.clock_label.get_text(),_[28])
		clock_edit.set_position(Gtk.PositionType.BOTTOM)
		clock_edit.set_relative_to(widget)
		clock_edit.show_all()
		clock_edit.popup()

	def date_edit_pop(self,widget,_):
		date_edit = DateEdit(self,self.date_label.get_text(),_[29])
		date_edit.set_position(Gtk.PositionType.BOTTOM)
		date_edit.set_relative_to(widget)
		date_edit.show_all()
		date_edit.popup()

	def display_clock(self):
		now = datetime.now()
		self.clock_label.set_text(now.strftime("%H:%M:%S"))
		return True

	def get_datetime_utc(self):
		self.datetime_utc = ayguci.get_datetime_utc().strip()
		if self.datetime_utc == "true":
			self.utc_switch.set_active(True)
			self.clock_edit.set_sensitive(False)
			self.date_edit.set_sensitive(False)
		elif self.datetime_utc == "false":
			self.utc_switch.set_active(False)
			self.clock_edit.set_sensitive(True)
			self.date_edit.set_sensitive(True)
		self.utc_switch.set_sensitive(self.datetime_utc)

	def network_switch_change(self,widget,state):
		state = widget.get_active()
		if state:
			self.utc_switch.set_active(False)
			self.date_edit.set_sensitive(False)
			self.clock_edit.set_sensitive(False)
		else:
			if not self.utc_switch.get_active():
				self.date_edit.set_sensitive(True)
				self.clock_edit.set_sensitive(True)


	def switch_change(self,widget,state):
		state = widget.get_active()
		if state:
			self.network_time_switch.set_active(False)
			self.date_edit.set_sensitive(False)
			self.clock_edit.set_sensitive(False)
		else:
			if not self.network_time_switch.get_active():
				self.date_edit.set_sensitive(True)
				self.clock_edit.set_sensitive(True)


	def get_timezone(self):
		self.active_coordinat = ayguci.get_timezone()
		if self.active_coordinat == "Turkey":
			self.active_coordinat = "Europe/Istanbul"
		self.map_select = self.active_coordinat.split("/")


	def apply_button_click(self,widget):
		if self.active_coordinat == "Europe/Istanbul":
			ayguci.set_timezone("Turkey")
		else:
			ayguci.set_timezone(self.active_coordinat)
		if self.utc_switch.get_active():
			ayguci.set_datetime_utc("true")
		else:
			ayguci.set_datetime_utc("false")
		if self.network_time_switch.get_active():
			ayguci.activate_ntp()
		else:
			ayguci.deactivate_ntp()
		# Anlamsız bir şekilde saatin değişmesi için Gtk.Warning gerekiyor
		# Her ne kadar sa.ma bir yöntemde olsa işe yaraması için
		# İconu tekrar yükleyip warning alıyoruz.
		img = Gtk.Image.new_from_stock(Gtk.STOCK_EXECUTE,36)
		self.clock_edit.set_image(img)
		

	def map_click(self,widget,pos):
		d = 2000
		coordinat = False
		for i in list(self.pixel_coordiant.keys()):
			d_ = (i[0]-pos.x)**2 + (i[1]-pos.y)**2
			if d_ < d:
				d = d_
				coordinat = (i[0],i[1])
		if coordinat:
			zone = self.pixel_coordiant[coordinat].split("/")
			country = "/".join(zone[1:])
			self.map_select = [zone[0],country]
			self.zone_combo_update()


	def get_coordinats(self):
		self.pixel_coordiant = {}
		for zone, coordinat in coordinats.coordinats.items():
			pos = self.get_coordinat_pixels(coordinat[1], coordinat[0], zone)
			self.pixel_coordiant[pos[0]] = pos[1]

		self.editing_countrys = {}
		for i in self.pixel_coordiant.items():
			change = i[1].split("/")
			if len(change) == 2:
				varmi = self.editing_countrys.get(change[0], "yok")
				if varmi == "yok":
					self.editing_countrys[change[0]] = [change[1]]
				else:
					self.editing_countrys[change[0]].append(change[1])
			elif len(change) == 3:
				varmi = self.editing_countrys.get(change[0], "yok")
				if varmi == "yok":
					self.editing_countrys[change[0]] = [change[1]+"/"+change[2]]
				else:
					self.editing_countrys[change[0]].append(change[1]+"/"+change[2])
			else:
				print(i)
		self.zone_combo_update()

	def get_coordinat_pixels(self, longitude, latitude, time_zone=""):
		width = 630
		height = 315
		x = (width/2)*(longitude/180)+(width/2)
		y = (height/2)-((height/2)*(latitude/90))
		return ((x, y), time_zone)

	def zone_combo_update(self):
		s = 0
		control = True
		self.zone_combo.remove_all()
		for sehir in list(self.editing_countrys.keys()):
			self.zone_combo.append_text(sehir)
			if self.map_select[0] == sehir:
				if self.zone_combo.get_active_text() == sehir:
					self.zone_combo_change(None)
				else:
					self.zone_combo.set_active(s)
				control = False
			s += 1
		if self.map_select[0] == "" and control:
			self.zone_combo.set_active(0)

	def country_combo_change(self,widget):
		if self.country_combo.get_active_text() != None:
			zone_name = self.zone_combo.get_active_text()+"/"+self.country_combo.get_active_text()
			self.active_coordinat = zone_name
			self.map.queue_draw()

	def zone_combo_change(self, widget):
		self.country_combo.remove_all()
		s = 0
		is_hire = False
		if self.zone_combo.get_active_text() == None:
			return
		for zone in self.editing_countrys[self.zone_combo.get_active_text()]:
			self.country_combo.append_text(zone)
			if zone == self.map_select[1]:
				self.country_combo.set_active(s)
				is_hire = True
			elif zone == "Istanbul" and self.map_select[1] == "":
				self.country_combo.set_active(s)
				is_hire = True
			s += 1
		if is_hire == False:
			self.country_combo.set_active(0)
		self.map_select = ["",""]

	def pin_calc(self):
		pos = coordinats.coordinats[self.active_coordinat]
		coordinat = self.get_coordinat_pixels(pos[-1], pos[0])
		x = coordinat[0][0] - 5
		y = coordinat[0][1] - 5
		return (x,y)

	def expose(self, widget, cr):
		map_ = cairo.ImageSurface.create_from_png(os.path.dirname(sys.argv[0]) + "/icons/map.png")
		cr.set_source_surface(map_,0,0)
		cr.paint()
		if self.active_coordinat:
			pin = cairo.ImageSurface.create_from_png(os.path.dirname(sys.argv[0]) + "/icons/pin.png")
			pin_konum = self.pin_calc()
			cr.set_source_surface(pin,pin_konum[0],pin_konum[1])
			cr.paint()

	def get_service_status(self,service):
		get = ayguci.run_process("servis/bil?servis="+service)
		get = get.split("\n")
		for g in get:
			if "[+]" in g:
				g = g.replace("[+]","")
				g = g.replace("  *   Status ","")
				return (True ,g)
			elif "[X]" in g:
				g = g.replace("[X]","")
				g = g.replace("  *   Status ","")
				return (False ,g)
		return (False,"")
