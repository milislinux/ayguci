import gi, os, subprocess
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject, Gio, GLib

class PopupQuestion(Gtk.Window):
	def __init__(self,parent,_,new_settings,old_settings,all_settings):
		Gtk.Window.__init__(self)
		self.parent = parent

		self.wf_settings = os.path.expanduser("~/.config/masa.ini")
		self.g_settings = GLib.KeyFile()
		if os.path.exists(self.wf_settings):
			self.g_settings.load_from_file(self.wf_settings,GLib.KeyFileFlags.NONE)
		else:
			self.g_settings = False

		main_box = Gtk.VBox()
		self.add(main_box)
		self.old_settings = old_settings
		self.new_settings = new_settings
		self.all_settings = all_settings

		self.info_label = Gtk.Label()
		self.time = 5
		self.info_label.set_text(_[87].format(self.time))
		main_box.pack_start(self.info_label,False,False,5)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		self.save_button = Gtk.Button()
		self.save_button.connect("clicked",self.save_kanshi)
		self.save_button.set_label(_[85])
		c_box.pack_start(self.save_button,True,True,5)

		self.cancel_button = Gtk.Button()
		self.cancel_button.connect("clicked",self.go_back)
		self.cancel_button.set_label(_[86])
		c_box.pack_start(self.cancel_button,True,True,5)
		self.timer = GObject.timeout_add(1000, self.timer,_)

	def timer(self,_):
		self.time -= 1
		if self.time == -1:
			self.go_back()
			return False
		self.info_label.set_text(_[87].format(self.time))
		return True

	def go_back(self,widget=None):
		e = self.old_settings[1]
		if e == "--on":
			e = "{}@{}".format(self.old_settings[2],self.old_settings[3])
		else:
			e = "off"
		if self.g_settings:
			s_name = "output:{}".format(self.old_settings[0])
			self.g_settings.set_string(s_name,"mode",e)
			self.g_settings.set_string(s_name,"scale",self.old_settings[7])
			self.g_settings.set_string(s_name,"transform",self.old_settings[6])
			self.g_settings.set_string(s_name,"position","{},{}".format(self.old_settings[4],self.old_settings[5]))
			self.g_settings.save_to_file(self.wf_settings)
			wlr_randr_command = "wlr-randr --output {} {} --mode {}@{}Hz --pos {},{} --transform {} --scale {}".format(*self.old_settings)
			os.system(wlr_randr_command)
		self.parent.screen_info = self.parent.get_screen_info()
		self.parent.set_screens()
		self.destroy()

	def save_kanshi(self,widget):
		GObject.source_remove(self.timer)
		self.parent.screen_info = self.parent.get_screen_info()
		self.parent.set_screens()
		self.destroy()
				


class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_):
		Gtk.Window.__init__(self)
		self._ = _
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		self.name = _[74]
		self.title = _[74]

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("screensaver",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		main_box = Gtk.VBox()
		self.add(main_box)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		screens_label = Gtk.Label()
		screens_label.set_text(_[81])
		c_box.pack_start(screens_label,False,False,5)
		self.screens_combo = Gtk.ComboBoxText()
		self.screens_combo.connect("changed",self.screen_combo_change)
		c_box.pack_start(self.screens_combo,True,True,5)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		desc_lb = Gtk.Label()
		desc_lb.set_text(_[82])
		c_box.pack_start(desc_lb,False,True,5)
		self.desc_lb = Gtk.Label()
		c_box.pack_start(self.desc_lb,True,True,5)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		ps_size = Gtk.Label()
		ps_size.set_text(_[75])
		c_box.pack_start(ps_size,False,True,5)
		self.ps_size_lb = Gtk.Label()
		c_box.pack_start(self.ps_size_lb,True,True,5)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		enabled_lb = Gtk.Label()
		enabled_lb.set_text(_[76])
		c_box.pack_start(enabled_lb,False,True,5)
		self.enabled_cb = Gtk.CheckButton()
		c_box.pack_start(self.enabled_cb,True,True,5)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		modes_label = Gtk.Label()
		modes_label.set_text(_[80])
		c_box.pack_start(modes_label,False,False,5)
		self.modes_combo = Gtk.ComboBoxText()
		c_box.pack_start(self.modes_combo,True,True,5)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		position_lb = Gtk.Label()
		position_lb.set_text(_[77])
		c_box.pack_start(position_lb,False,True,5)
		position_lb_x = Gtk.Label("x")
		c_box.pack_start(position_lb_x,False,True,5)
		ad = Gtk.Adjustment(0, 0, 7680, 1, 0, 0)
		self.pos_x_sb = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		c_box.pack_start(self.pos_x_sb,True,True,5)
		position_lb_y = Gtk.Label("y")
		c_box.pack_start(position_lb_y,False,True,5)
		ad = Gtk.Adjustment(0, 0, 4320, 1, 0, 0)
		self.pos_y_sb = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		c_box.pack_start(self.pos_y_sb,True,True,5)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		self.transforms_list = ["normal","90","180","270","flipped","flipped-90","flipped-180","flipped-270"]
		transform_label = Gtk.Label()
		transform_label.set_text(_[78])
		c_box.pack_start(transform_label,False,False,5)
		self.transform_combo = Gtk.ComboBoxText()
		c_box.pack_start(self.transform_combo,True,True,5)
		for tf in self.transforms_list:
			self.transform_combo.append_text(tf)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)
		scale_lb = Gtk.Label()
		scale_lb.set_text(_[79])
		c_box.pack_start(scale_lb,False,True,5)
		ad = Gtk.Adjustment(0, 0, 5, 0.1, 0, 0)
		self.scale_sb = Gtk.SpinButton(adjustment=ad, climb_rate=0.1, digits=1)
		c_box.pack_start(self.scale_sb,True,True,5)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,False,False,5)

		self.refresh_button = Gtk.Button()
		self.refresh_button.set_label(_[84])
		self.refresh_button.connect("clicked",self.refresh_screens)
		c_box.pack_start(self.refresh_button,True,True,5)

		self.apply_button = Gtk.Button()
		self.apply_button.set_label(_[20])
		self.apply_button.connect("clicked",self.apply_changes)
		c_box.pack_start(self.apply_button,True,True,5)

		self.screen_info = self.get_screen_info()
		self.set_screens()

	def apply_changes(self,widget):
		screen_name = self.screens_combo.get_active_text()
		enabled = self.enabled_cb.get_active()
		if enabled:
			enabled = "--on"
		else:
			enabled = "--off"
		mode = self.modes_combo.get_active_text()
		mode = mode.split()
		mode_size = mode[0]
		mode_hz = mode[2]
		pos_x = int(self.pos_x_sb.get_value())
		pos_y = int(self.pos_y_sb.get_value())
		transform = self.transform_combo.get_active_text()
		scale = self.scale_sb.get_value()
		new_settings = (screen_name,enabled,mode_size,mode_hz,pos_x,pos_y,transform,str(scale))
		wlr_randr_command = "wlr-randr --output {} {} --mode {}@{}Hz --pos {},{} --transform {} --scale {}".format(*new_settings)
		wf_settings = os.path.expanduser("~/.config/masa.ini")
		g_settings = GLib.KeyFile()
		if os.path.exists(wf_settings):
			g_settings.load_from_file(wf_settings,GLib.KeyFileFlags.NONE)
		else:
			g_settings = False

		e = new_settings[1]
		if e == "--on":
			e = "{}@{}".format(new_settings[2],new_settings[3])
		else:
			e = "off"
		if g_settings:
			s_name = "output:{}".format(new_settings[0])
			g_settings.set_string(s_name,"mode",e)
			g_settings.set_string(s_name,"scale",new_settings[7])
			g_settings.set_string(s_name,"transform",new_settings[6])
			g_settings.set_string(s_name,"position","{},{}".format(new_settings[4],new_settings[5]))
			g_settings.save_to_file(wf_settings)
			os.system(wlr_randr_command)


		for info in self.screen_info:
			if info["name"] == screen_name:
				if info["enabled"] == "yes":
					enabled = "--on"
				else:
					enabled = "--off"
				for mode in info["modes"]:
					self.modes_combo.append_text(mode)
					if "current" in mode:
						mode = mode.split()
						mode_size = mode[0]
						mode_hz = mode[2]
				pos_x, pos_y = info["position"].split(",")
				old_settings = (info["name"],enabled,mode_size,mode_hz,pos_x,pos_y,info["transform"],info["scale"])
				break
		win = PopupQuestion(self,self._,new_settings,old_settings,self.screen_info)
		win.show_all()

	def parse_settings(screen_name,enabled,mode,pos,transform,scale):
		if enabled == False or enabled == "no":
			enabled = "--off"
		else:
			enabled = "--on"
		mode = self.modes_combo.get_active_text()
		mode = mode.split()
		mode_size = mode[0]
		mode_hz = mode[2]
		pos_x, pos_y = pos.split(",")
		return (screen_name,enabled,mode_size,mode_hz,pos_x,pos_y,transform,scale)

	def refresh_screens(self,widget):
		self.screen_info = self.get_screen_info()
		self.set_screens()

	def screen_combo_change(self,widget):
		screen = widget.get_active_text()
		self.set_screen_info(screen)

	def set_screens(self):
		s = 0
		self.screens_combo.remove_all()
		for info in self.screen_info:
			self.screens_combo.append_text(info["name"])
			self.screens_combo.set_active(s)
			s += 1
			if info["enabled"] == "yes":
				self.set_screen_info(info["name"])


	def set_screen_info(self,screen_name):
		for info in self.screen_info:
			if info["name"] == screen_name:
				self.desc_lb.set_text(info["desc"])
				self.ps_size_lb.set_text(info["physical"])
				if info["enabled"] == "yes":
					self.enabled_cb.set_active(1)
				else:
					self.enabled_cb.set_active(0)
				self.modes_combo.remove_all()
				for mode in info["modes"]:
					self.modes_combo.append_text(mode)
					if "current" in mode:
						self.modes_combo.set_active(info["modes"].index(mode))
				if "," in info["position"]:
					x,y = info["position"].split(",")
				else:
					x,y = ("0","0")
				self.pos_x_sb.set_value(int(x))
				self.pos_y_sb.set_value(int(y))
				self.transform_combo.set_active(self.transforms_list.index(info["transform"]))
				self.scale_sb.set_value(float(info["scale"]))
				break

	def get_screen_info(self):
		get = subprocess.Popen(["wlr-randr"], stdout = subprocess.PIPE)
		get.wait()
		get = get.communicate()[0]
		get = get.decode("utf-8","strict")
		get = get.split("\n")
		screens = []
		s = False
		for g in get:
			if g != "" and g[0] != " ":
				if s:
					s["modes"].sort()
					screens.append(s)
				s = {"name":"","desc":"","physical":"","enabled":"","modes":[],"position":"","transform":"","scale":""}
				g = g.split(' "')
				s["name"] = g[0]
				s["desc"] = g[1][:-1]
			elif "Physical size" in g:
				p_s = g.replace(" ","")
				p_s = p_s.replace("Physicalsize:","")
				s["physical"] = p_s
			elif "Enabled:" in g:
				e = g.replace(" ","")
				e = e.replace("Enabled:","")
				s["enabled"] = e
			elif "Position:" in g:
				p = g.replace(" ","")
				p = p.replace("Position:","")
				s["position"] = p
			elif "Transform:" in g:
				t = g.replace(" ","")
				t = t.replace("Transform:","")
				s["transform"] = t
			elif "Scale:" in g:
				sc = g.replace(" ","")
				sc = sc.replace("Scale:","")
				s["scale"] = sc
			elif " px, " in g:
				s["modes"].append(g[4:])
		if s not in screens:
			s["modes"].sort()
			screens.append(s)
		return screens
