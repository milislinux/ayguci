import gi, os, json, subprocess#, subprocess
from gi.repository import Gtk, GLib, Gdk#, GdkPixbuf

class ModuleAdd(Gtk.Popover):
	def __init__(self,parent,modules,position,_):
		super(ModuleAdd,self).__init__()
		m_box = Gtk.VBox()
		self.add(m_box)
		self.parent = parent
		if len(modules) == 0:
			m_box.pack_start(Gtk.Label(_[121]),0,0,5)
		else:
			for module in modules:
				btn = Gtk.Button()
				btn.set_label(module)
				btn.connect("clicked",self.add_module,module,position)
				m_box.pack_start(btn,0,0,5)

	def add_module(self,widget,module,position):
		if position == "left":
			self.parent.waybar_dict["modules-left"].append(module)
		elif position == "center":
			self.parent.waybar_dict["modules-center"].append(module)
		else:
			self.parent.waybar_dict["modules-right"].append(module)
		self.parent.set_modules()
		self.destroy()


class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,json_parse,_):
		Gtk.Window.__init__(self)
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		main_box = Gtk.VBox()
		self.add(main_box)

		self.name = _[114]
		self.title = _[114]

		self.TARGETS = [('MY_TREE_MODEL_ROW', Gtk.TargetFlags.SAME_WIDGET, 0),
						('text/plain', 0, 1),
						('TEXT', 0, 2),
						('STRING', 0, 3)]

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("gnome-panel",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		self.waybar_dict = json_parse

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,0,0,5)

		c_box.pack_start(Gtk.Label(_[115]),0,0,5)
		self.position_cb = Gtk.ComboBoxText()
		self.positions = ["top","bottom","left","right"]
		for text in self.positions:
			self.position_cb.append_text(text)
		c_box.pack_start(self.position_cb,1,1,5)

		c_box.pack_start(Gtk.Label(_[116]),0,0,5)
		ad = Gtk.Adjustment(0, 0, 300, 1, 0, 0)
		self.height_sb = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.height_sb.set_wrap(True)
		c_box.pack_start(self.height_sb,1,1,5)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,0,0,5)

		self.left_btn = Gtk.Button()
		self.left_btn.connect("clicked",self.btn_clicked,"left",_)
		self.left_btn.set_label(_[120])
		img = Gtk.Image.new_from_stock(Gtk.STOCK_ADD,36)
		self.left_btn.set_image(img)
		c_box.pack_start(self.left_btn,1,1,5)

		self.center_btn = Gtk.Button()
		self.center_btn.connect("clicked",self.btn_clicked,"center",_)
		self.center_btn.set_label(_[120])
		img = Gtk.Image.new_from_stock(Gtk.STOCK_ADD,36)
		self.center_btn.set_image(img)
		c_box.pack_start(self.center_btn,1,1,5)

		self.right_btn = Gtk.Button()
		self.right_btn.connect("clicked",self.btn_clicked,"right",_)
		self.right_btn.set_label(_[120])
		img = Gtk.Image.new_from_stock(Gtk.STOCK_ADD,36)
		self.right_btn.set_image(img)
		c_box.pack_start(self.right_btn,1,1,5)

		c_box = Gtk.HBox()
		main_box.pack_start(c_box,1,1,5)

		self.left_store = Gtk.ListStore(str)
		self.left_iw = Gtk.TreeView(model=self.left_store)
		self.left_iw.set_activate_on_single_click(True)
		self.left_iw.enable_model_drag_source(Gdk.ModifierType.BUTTON1_MASK,self.TARGETS, Gdk.DragAction.DEFAULT|Gdk.DragAction.MOVE)
		self.left_iw.enable_model_drag_dest(self.TARGETS, Gdk.DragAction.DEFAULT)
		self.left_iw.drag_dest_add_text_targets()
		self.left_iw.drag_source_add_text_targets()
		self.left_iw.connect("drag_data_get", self.drag_data_get_data)
		self.left_iw.connect("drag_data_received", self.drag_data_received_data,"left")
		self.left_iw.connect("button-press-event",self.iw_activate,_,"left")
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[117],renderer, text = 0)
		self.left_iw.append_column(coloumn)
		c_box.pack_start(self.set_scroll_win(self.left_iw),1,1,5)

		self.center_store = Gtk.ListStore(str)
		self.center_iw = Gtk.TreeView(model=self.center_store)
		self.center_iw.set_activate_on_single_click(True)
		self.center_iw.enable_model_drag_source(Gdk.ModifierType.BUTTON1_MASK,self.TARGETS, Gdk.DragAction.DEFAULT|Gdk.DragAction.MOVE)
		self.center_iw.enable_model_drag_dest(self.TARGETS, Gdk.DragAction.DEFAULT)
		self.center_iw.drag_dest_add_text_targets()
		self.center_iw.drag_source_add_text_targets()
		self.center_iw.connect("drag_data_get", self.drag_data_get_data)
		self.center_iw.connect("drag_data_received", self.drag_data_received_data,"center")
		self.center_iw.connect("button-press-event",self.iw_activate,_,"center")
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[118],renderer, text = 0)
		self.center_iw.append_column(coloumn)
		c_box.pack_start(self.set_scroll_win(self.center_iw),1,1,5)

		self.right_store = Gtk.ListStore(str)
		self.right_iw = Gtk.TreeView(model=self.right_store)
		#self.right_iw.set_activate_on_single_click(True)
		self.right_iw.enable_model_drag_source(Gdk.ModifierType.BUTTON1_MASK,self.TARGETS, Gdk.DragAction.DEFAULT|Gdk.DragAction.MOVE)
		self.right_iw.enable_model_drag_dest(self.TARGETS, Gdk.DragAction.DEFAULT)
		self.right_iw.drag_dest_add_text_targets()
		self.right_iw.drag_source_add_text_targets()
		self.right_iw.connect("drag_data_get", self.drag_data_get_data)
		self.right_iw.connect("drag_data_received", self.drag_data_received_data,"right")
		self.right_iw.connect("button-press-event",self.iw_activate,_,"right")
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[119],renderer, text = 0)
		self.right_iw.append_column(coloumn)
		c_box.pack_start(self.set_scroll_win(self.right_iw),1,1,5)


		c_box = Gtk.HBox()
		main_box.pack_start(c_box,0,0,5)

		self.apply_btn = Gtk.Button()
		self.apply_btn.connect("clicked",self.apply_changes)
		self.apply_btn.set_label(_[20])
		img = Gtk.Image.new_from_stock(Gtk.STOCK_APPLY,36)
		self.apply_btn.set_image(img)
		c_box.pack_start(self.apply_btn,1,1,5)


		self.set_waybar_settings()

	def apply_changes(self,widget):
		self.waybar_dict["height"] = int(self.height_sb.get_text())
		self.waybar_dict["position"] = self.position_cb.get_active_text()
		with open(os.path.expanduser("~/.config/waybar/config"),"w") as f:
			json.dump(self.waybar_dict, f, indent=4, ensure_ascii=False)
		subprocess.Popen(["killall","waybar"], stdout = subprocess.PIPE)
		GLib.timeout_add(1000,self.run_waybar)

	def run_waybar(self):
		subprocess.Popen(["waybar"], stdout = subprocess.PIPE)
		return False

	def drag_data_get_data(self, treeview, context, selection, target_id,etime):
		treeselection = treeview.get_selection()
		model, iter = treeselection.get_selected()
		data = bytes(model.get_value(iter, 0), "utf-8")
		selection.set(selection.get_target(), 8, data)

	def drag_data_received_data(self, treeview, context, x, y, selection, info, etime, pos):
		model = treeview.get_model()
		data = selection.get_data().decode("utf-8")
		drop_info = treeview.get_dest_row_at_pos(x, y)
		if drop_info:
			path, position = drop_info
			iter = model.get_iter(path)
			if (position == Gtk.TreeViewDropPosition.BEFORE or position == Gtk.TreeViewDropPosition.BEFORE):
				model.insert_before(iter, [data])
			else:
				model.insert_after(iter, [data])
		else:
			model.append([data])
		if context.get_actions() == Gdk.DragAction.MOVE:
			context.finish(True, True, etime)
		if data in self.waybar_dict["modules-left"]:
			self.waybar_dict["modules-left"].remove(data)
		elif data in self.waybar_dict["modules-center"]:
			self.waybar_dict["modules-center"].remove(data)
		elif data in self.waybar_dict["modules-right"]:
			self.waybar_dict["modules-right"].remove(data)

		if pos == "left":
			self.waybar_dict["modules-left"] = []
			for mod in model:
				self.waybar_dict["modules-left"].append(mod[0])
		elif pos == "center":
			self.waybar_dict["modules-center"] = []
			for mod in model:
				self.waybar_dict["modules-center"].append(mod[0])
		elif pos == "right":
			self.waybar_dict["modules-right"] = []
			for mod in model:
				self.waybar_dict["modules-right"].append(mod[0])
		return


	def iw_activate(self,widget,event,_,position):
		"""Burada kaldık silme ve yer değiştirme işlevleri yapılacak
		FTERM den bakabilirsin."""
		if event.type == Gdk.EventType.BUTTON_PRESS and event.button == 3:
			if position == "left":
				tree_model = self.left_iw.get_model()
				modules = self.waybar_dict["modules-left"]
			elif position == "center":
				tree_model = self.center_iw.get_model()
				modules = self.waybar_dict["modules-center"]
			elif position == "right":
				tree_model = self.right_iw.get_model()
				modules = self.waybar_dict["modules-right"]
			path, coloumn, cel_x, cel_y = widget.get_path_at_pos(event.x, event.y)
			if path != None and tree_model != None:
				select = tree_model[path]
				menu = Gtk.Menu()
				if select[0] != modules[0]:
					menu_item = Gtk.ImageMenuItem(_[122])
					img = Gtk.Image.new_from_stock(Gtk.STOCK_GOTO_TOP,Gtk.IconSize.MENU)
					menu_item.set_image(img)
					menu_item.connect("activate",self.module_sort_up, select[0],position)
					menu.add(menu_item)
				if select[0] != modules[-1]:
					menu_item = Gtk.ImageMenuItem(_[123])
					img = Gtk.Image.new_from_stock(Gtk.STOCK_GOTO_BOTTOM,Gtk.IconSize.MENU)
					menu_item.set_image(img)
					menu_item.connect("activate",self.module_sort_down, select[0],position)
					menu.add(menu_item)
				menu_item = Gtk.ImageMenuItem(_[124])
				img = Gtk.Image.new_from_stock(Gtk.STOCK_REMOVE,Gtk.IconSize.MENU)
				menu_item.set_image(img)
				menu_item.connect("activate",self.module_remove, select[0],position)
				menu.add(menu_item)
				menu.attach_to_widget(widget)
				menu.show_all()
				menu.popup_at_pointer()

	def module_remove(self,widget,item,position):
		if position == "left":
			self.waybar_dict["modules-left"].remove(item)
		elif position == "center":
			self.waybar_dict["modules-center"].remove(item)
		elif position == "right":
			self.waybar_dict["modules-right"].remove(item)
		self.set_modules()

	def module_sort_up(self,widget,item,position):
		if position == "left":
			index = self.waybar_dict["modules-left"].index(item)
			self.waybar_dict["modules-left"].insert(index-1,item)
			self.waybar_dict["modules-left"].pop(index+1)
		elif position == "center":
			index = self.waybar_dict["modules-center"].index(item)
			self.waybar_dict["modules-center"].insert(index-1,item)
			self.waybar_dict["modules-center"].pop(index+1)
		elif position == "right":
			index = self.waybar_dict["modules-right"].index(item)
			self.waybar_dict["modules-right"].insert(index-1,item)
			self.waybar_dict["modules-right"].pop(index+1)
		self.set_modules()

	def module_sort_down(self,widget,item,position):
		if position == "left":
			index = self.waybar_dict["modules-left"].index(item)
			self.waybar_dict["modules-left"].insert(index+2,item)
			self.waybar_dict["modules-left"].pop(index)
		elif position == "center":
			index = self.waybar_dict["modules-center"].index(item)
			self.waybar_dict["modules-center"].insert(index+2,item)
			self.waybar_dict["modules-center"].pop(index)
		elif position == "right":
			index = self.waybar_dict["modules-right"].index(item)
			self.waybar_dict["modules-right"].insert(index+2,item)
			self.waybar_dict["modules-right"].pop(index)
		self.set_modules()

	def btn_clicked(self,widget,position,_):
		add_ = ModuleAdd(self,self.get_add_modules(),position,_)
		add_.set_position(Gtk.PositionType.BOTTOM)
		add_.set_relative_to(widget)
		add_.show_all()
		add_.popup()

	def get_add_modules(self):
		all_modules = list(self.waybar_dict.keys())
		all_modules.remove("position")
		all_modules.remove("height")
		all_modules.remove("modules-left")
		all_modules.remove("modules-center")
		all_modules.remove("modules-right")
		all_modules = self.remove_added_modules(all_modules,self.waybar_dict["modules-left"])
		all_modules = self.remove_added_modules(all_modules,self.waybar_dict["modules-center"])
		all_modules = self.remove_added_modules(all_modules,self.waybar_dict["modules-right"])
		return all_modules


	def set_scroll_win(self,list_):
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(list_)
		return scroll

	def remove_added_modules(self, all_modules,modules):
		for module in modules:
			if module in all_modules:
				all_modules.remove(module)
		return all_modules


	def append_store(self,store,modules):
		store.clear()
		for module in modules:
			store.append([module])

	def set_modules(self):
		self.append_store(self.left_store,self.waybar_dict["modules-left"])
		self.append_store(self.center_store,self.waybar_dict["modules-center"])
		self.append_store(self.right_store,self.waybar_dict["modules-right"])

	def set_waybar_settings(self):
		self.position_cb.set_active(self.positions.index(self.waybar_dict["position"]))
		self.height_sb.set_value(int(self.waybar_dict["height"]))
		self.set_modules()
