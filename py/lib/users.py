import gi, os, subprocess, getpass
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject, Gio
from lib import ayguci, pam

class ChangeUserPasswd(Gtk.Window):
	def __init__(self,parent,user_name,_):
		super(ChangeUserPasswd,self).__init__()
		self.parent = parent
		self.set_title(_[89].format(user_name))
		self.user_name = user_name
		self.open_user_name = getpass.getuser()
		m_box = Gtk.VBox()
		self.add(m_box)

		if self.user_name == self.open_user_name:
			h_box = Gtk.HBox()
			m_box.pack_start(h_box,1,1,5)
			old_pswd_lb = Gtk.Label()
			old_pswd_lb.set_text(_[90])
			h_box.pack_start(old_pswd_lb,1,1,5)
			self.old_pswd_e = Gtk.Entry()
			self.old_pswd_e.set_visibility(False)
			h_box.pack_start(self.old_pswd_e,1,1,5)

		h_box = Gtk.HBox()
		m_box.pack_start(h_box,1,1,5)
		n_pswd_1_lb = Gtk.Label()
		n_pswd_1_lb.set_text(_[91])
		h_box.pack_start(n_pswd_1_lb,1,1,5)
		self.n_pswd_1_e = Gtk.Entry()
		self.n_pswd_1_e.set_visibility(False)
		h_box.pack_start(self.n_pswd_1_e,1,1,5)

		h_box = Gtk.HBox()
		m_box.pack_start(h_box,1,1,5)
		n_pswd_2_lb = Gtk.Label()
		n_pswd_2_lb.set_text(_[92])
		h_box.pack_start(n_pswd_2_lb,1,1,5)
		self.n_pswd_2_e = Gtk.Entry()
		self.n_pswd_2_e.set_visibility(False)
		h_box.pack_start(self.n_pswd_2_e,1,1,5)
		
		apply_btn = Gtk.Button()
		apply_btn.connect("clicked",self.change_passwd,user_name,_)
		apply_btn.set_label(_[20])
		m_box.pack_start(apply_btn,1,1,5)

	def ayguci_change_pwd(self,newpass,auth_pass,_):
		c_h = ayguci.change_user_pass(self.user_name,newpass,self.open_user_name,auth_pass)
		print(c_h)
		if "change" in c_h  and c_h["change"] == 'ok':
			self.parent.create_info_dialog(_[82],_[99])
			self.destroy()
		else:
			self.parent.create_info_dialog(_[38],c_h["error"])

	def change_passwd(self,widget,user_name,_):
		if self.open_user_name == self.user_name:
			o_passwd = self.old_pswd_e.get_text()
		n_1_passwd = self.n_pswd_1_e.get_text()
		n_2_passwd = self.n_pswd_2_e.get_text()
		if (self.open_user_name == self.user_name and o_passwd == "") or n_1_passwd == "" or n_2_passwd == "":
			self.parent.create_info_dialog(_[38],_[93])
		elif n_1_passwd != n_2_passwd:
			self.parent.create_info_dialog(_[38],_[94])
		else:
			# aktif kullanıcı
			if self.open_user_name == self.user_name:
				p = pam.pam()
				is_ok = p.authenticate(self.user_name,o_passwd)
				if is_ok:
					self.ayguci_change_pwd(n_1_passwd,o_passwd,_)
				else:
					self.parent.create_info_dialog(_[38],_[98])
			# diğer kullanıcı
			else:
				p = pam.pam()
				o_passwd =  p.auth_gui()
				if o_passwd:
					self.ayguci_change_pwd(n_1_passwd,o_passwd,_)
				else:
					self.parent.create_info_dialog(_[38],_[98])

class UserAddEdit(Gtk.Popover):
	def __init__(self,parent,all_groups,default_groups,add_edit,desc,_):
		super(UserAddEdit,self).__init__()
		self.select_groups = default_groups
		self.add_edit = add_edit
		m_box = Gtk.VBox()
		self.add(m_box)
		self.parent = parent

		box = Gtk.HBox()
		u_name_label = Gtk.Label()
		u_name_label.set_text(_[41])
		box.pack_start(u_name_label,0,0,5)
		self.user_name = Gtk.Entry()
		box.pack_start(self.user_name,1,1,5)
		m_box.pack_start(box,1,1,5)

		box = Gtk.HBox()
		desc_label = Gtk.Label()
		desc_label.set_text(_[36])
		box.pack_start(desc_label,0,0,5)
		self.desc = Gtk.Entry()
		box.pack_start(self.desc,1,1,5)
		m_box.pack_start(box,1,1,5)

		box = Gtk.HBox()
		h_s_label = Gtk.Label()
		h_s_label.set_text(_[42])
		self.home_switch = Gtk.Switch()
		if add_edit == "add":
			box.pack_start(h_s_label,0,0,5)
			box.pack_end(self.home_switch,0,1,5)
		m_box.pack_start(box,1,1,5)

		if add_edit != "add":
			self.desc.set_text(desc)
			self.user_name.set_text(add_edit)
			self.user_name.set_sensitive(False)
		groups_box = Gtk.HBox()
		m_box.pack_start(groups_box,1,1,5)
		s = 0
		for groupo in all_groups:
			group = groupo["group_name"]
			if s % 10 == 0:
				v_box = Gtk.VBox()
				groups_box.pack_start(v_box,1,1,5)
			cb = Gtk.CheckButton()
			if default_groups and group in default_groups:
				cb.set_active(True)
			cb.connect("clicked",self.cb_click,group)
			cb.set_label(group)
			v_box.pack_start(cb,1,1,5)
			s += 1

		self.add_user_button = Gtk.Button()
		self.add_user_button.set_label(_[20])
		m_box.pack_start(self.add_user_button,1,1,5)

		if add_edit == "add":
			self.add_user_button.connect("clicked",self.add_user,_)
		else:
			self.add_user_button.connect("clicked",self.edit_user,_)
			self.del_user_button = Gtk.Button()
			self.del_user_button.connect("clicked",self.delete_user_dialog,_)
			self.del_user_button.set_label(_[44])
			m_box.pack_start(self.del_user_button,1,1,5)

			users = ayguci.get_normal_users()
			# kullanici kullanıcı dict içinde aranır
			filter_result = list(filter(lambda k: k["username"] == add_edit, users))
			# blunursa
			if len(filter_result) > 0:
				self.passwd_button = Gtk.Button()
				self.passwd_button.connect("clicked",self.passwd_user_change,add_edit,_)
				self.passwd_button.set_label(_[88])
				m_box.pack_start(self.passwd_button,1,1,5)

		self.user_name.connect("changed", self.user_name_change)
		# ayar merkezini açan kullanıcı
		self.opened_user = getpass.getuser()
		
	def passwd_user_change(self,widget,user_name,_):
		win = ChangeUserPasswd(self,user_name,_)
		win.show_all()

	def user_name_change(self,widget):
		self.desc.set_text(widget.get_text())

	def cb_click(self,widget,group):
		if group in self.select_groups:
			self.select_groups.remove(group)
		else:
			self.select_groups.append(group)

	def add_user(self,widget,_,edit=True):
		user_name = self.user_name.get_text()
		desc = self.desc.get_text()
		users = ayguci.get_all_users()
		if user_name == "":
			self.create_info_dialog(_[38],_[39])
		elif desc == "":
			self.create_info_dialog(_[38],_[48])
		elif user_name in users and edit:
			self.create_info_dialog(_[38],_[40])
		else:
			# bool home
			home =  self.home_switch.get_active()
			groups = " ".join(self.select_groups)
			# arayüzden alınacak
			shell = True
			p = pam.pam()
			passwd =  p.auth_gui()
			if passwd:
				ayguci.add_user(user_name,desc,home,shell,groups, self.opened_user, passwd)
			self.parent.get_users(self.parent.all_user_show.get_active())
			self.destroy()

	def edit_user(self,widget,_):
		self.add_user(None,_,False)

	def delete_user_dialog(self,widget,_):
		p = pam.pam()
		passwd =  p.auth_gui()
		if passwd: 
			q = Gtk.MessageDialog(None,0,Gtk.MessageType.QUESTION, Gtk.ButtonsType.OK_CANCEL,_[45])
			q.format_secondary_text(_[46].format(self.user_name.get_text()))
			ans = q.run()
			if ans == Gtk.ResponseType.OK:
				self.delete_user(passwd)
				self.parent.get_users(self.parent.all_user_show.get_active())
				q.destroy()
				self.destroy()
			else:
				q.destroy()

	def delete_user(self,passwd):
		user_name = self.user_name.get_text()
		ayguci.delete_user(user_name, self.opened_user, passwd)

	def create_info_dialog(self,title,text):
		dialog = Gtk.MessageDialog(None, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, title)
		dialog.format_secondary_text(text)
		dialog.run()
		dialog.destroy()

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent,_):
		Gtk.Window.__init__(self)
		self._ = _
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		self.name = _[30]
		self.title = _[30]

		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("user_icon",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None

		h_box = Gtk.HBox()
		main_box = Gtk.VBox()
		h_box.pack_start(main_box,True,True,10)
		self.add(h_box)

		self.user_add_button = Gtk.Button()
		self.user_add_button.connect("clicked",self.user_add)
		self.user_add_button.set_label(_[31])
		img = Gtk.Image.new_from_stock(Gtk.STOCK_ADD,36)
		self.user_add_button.set_image(img)
		main_box.pack_start(self.user_add_button,0,1,5)

		box = Gtk.HBox()
		main_box.pack_start(box,0,1,5)

		self.all_user_show_label = Gtk.Label()
		self.all_user_show_label.set_text(_[37])
		box.pack_start(self.all_user_show_label,0,1,5)

		self.all_user_show = Gtk.Switch()
		self.all_user_show.connect("notify::active", self.switch_change)
		box.pack_end(self.all_user_show,0,1,5)

		self.users_iw_store = Gtk.ListStore(GdkPixbuf.Pixbuf,str,str,str,str,str,str)
		self.users_iw = Gtk.TreeView(model=self.users_iw_store)
		self.users_iw.set_activate_on_single_click(True)
		self.users_iw.connect("row-activated",self.users_iw_activate)
		renderer = Gtk.CellRendererPixbuf()
		coloumn = Gtk.TreeViewColumn(_[1],renderer, gicon = 0)
		self.users_iw.append_column(coloumn)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn(_[43],renderer, text = 1)
		self.users_iw.append_column(coloumn)
		coloumn = Gtk.TreeViewColumn(_[32],renderer, text = 2)
		self.users_iw.append_column(coloumn)
		coloumn = Gtk.TreeViewColumn(_[35],renderer, text = 3)
		self.users_iw.append_column(coloumn)
		coloumn = Gtk.TreeViewColumn(_[33],renderer, text = 4)
		self.users_iw.append_column(coloumn)
		coloumn = Gtk.TreeViewColumn(_[34],renderer, text = 5)
		self.users_iw.append_column(coloumn)
		coloumn = Gtk.TreeViewColumn(_[36],renderer, text = 6)
		self.users_iw.append_column(coloumn)

		main_box.pack_start(self.set_scroll_win(self.users_iw),1,1,5)
		self.get_users()

	def switch_change(self,widget,state):
		state = widget.get_active()
		self.get_users(state)

	def user_add(self, widget):
		all_groups = ayguci.get_all_user_groups()
		default_groups = ayguci.get_default_user_groups()
		user_add_edit = UserAddEdit(self, all_groups, default_groups,"add","add",self._)
		user_add_edit.set_position(Gtk.PositionType.BOTTOM)
		user_add_edit.set_relative_to(widget)
		user_add_edit.show_all()
		user_add_edit.popup()

	def get_users(self, state = False):
		if state:
			users = ayguci.get_all_users()
		else:
			users = ayguci.get_normal_users()
		self.users_iw_store.clear()
		for user in users:
			self.users_iw_store.append([self.icon,user["username"],user["shell"],user["home"],user["uid"],user["gid"],user["comment"]])

	def set_scroll_win(self,list_):
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(list_)
		return scroll

	def users_iw_activate(self,widget,path,coloumn):
		select = self.users_iw.get_selection()
		tree_model, tree_iter = select.get_selected()
		if tree_iter != None and tree_model != None:
			select = tree_model[tree_iter]
			user = select[1]
			desc = select[6]
			all_groups = ayguci.get_all_user_groups()
			default_groups = ayguci.get_user_groups(user)
			user_add_edit = UserAddEdit(self, all_groups, default_groups,user,desc,self._)
			user_add_edit.set_position(Gtk.PositionType.BOTTOM)
			user_add_edit.set_relative_to(self.user_add_button)
			user_add_edit.show_all()
			user_add_edit.popup()
