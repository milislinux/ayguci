import subprocess, json, os, gi
from gi.repository import Gtk
from urllib import request, parse

win = False
user_passwd = None
ayguci_port = 5005

def create_debug_window():
	global win
	global debugview
	win = Gtk.Window()
	scrolledwindow = Gtk.ScrolledWindow()
	scrolledwindow.set_hexpand(True)
	scrolledwindow.set_vexpand(True)
	win.add(scrolledwindow)
	debugview = Gtk.TextView()
	scrolledwindow.add(debugview)
	win.set_size_request(640,480)
	win.show_all()

def run_process(arg):
	global user_passwd, ayguci_port
	ayg_api = f'curl -s http://localhost:{ayguci_port}/api/cmd/{arg}'
	cmd_get = subprocess.Popen(ayg_api.split(), stdout = subprocess.PIPE)
	cmd_get.wait()
	cmd_get = cmd_get.communicate()[0]
	cmd_get = cmd_get.decode("utf-8","strict")
	#print("-----",cmd_get)
	if win:
		db_buffer = debugview.get_buffer()
		end_iter = db_buffer.get_end_iter()
		db_buffer.insert(end_iter,"Komut >>>> {}\n".format(arg))
		end_iter = db_buffer.get_end_iter()
		db_buffer.insert(end_iter,"Dönüt <<<< {}\n".format(json.loads(cmd_get)))
	err = json.loads(cmd_get)["err"]
	if err:
		print("ERROR:",arg,err)
	return json.loads(cmd_get)["out"]

def get(route):
	global user_passwd, ayguci_port
	req = request.Request(
		f"http://localhost:{ayguci_port}/api/{route}", 
		method="GET",
	)	
	with request.urlopen(req) as resp:
		respo = json.loads(resp.read().decode())
		# debug
		if win:
			db_buffer = debugview.get_buffer()
			end_iter = db_buffer.get_end_iter()
			db_buffer.insert(end_iter,"İstek: {}\n".format(route))
			end_iter = db_buffer.get_end_iter()
			db_buffer.insert(end_iter,"Yanıt: {}\n".format(respo))
		# return			
		if "data" in respo:
			return respo["data"]
		if "err" in respo:
			print("ERROR:",respo["err"])
			return respo["err"]
	
	return None

def post(route, data_dict, method="POST"):
	global user_passwd, ayguci_port
	datax = json.dumps(data_dict).encode()
	req = request.Request(
		f"http://localhost:{ayguci_port}/api/{route}", 
		method=method,
		data=datax
	)
	with request.urlopen(req) as resp:
		respo = json.loads(resp.read().decode())
		# debug
		if win:
			db_buffer = debugview.get_buffer()
			end_iter = db_buffer.get_end_iter()
			db_buffer.insert(end_iter,"İstek: {}\n".format(route))
			end_iter = db_buffer.get_end_iter()
			db_buffer.insert(end_iter,"Yanıt: {}\n".format(respo))
		# return
		if "data" in respo:
			return respo["data"]
		if "err" in respo:
			print("ERROR:",respo["err"])
			return respo["err"]
	return None

def delete_user(username, auth_user, auth_passwd):	
	del_data = {
		"user" : username,
		"auth_user" : auth_user,
		"auth_passwd" : auth_passwd
	}
	resp = post("user", del_data, "DELETE")
	print(resp)

def get_all_user_groups():
	r_t = get("group")
	return r_t["groups"]

def get_user_groups(user_name):
	users = get("user")
	for user in filter(lambda k: k["username"] == user_name, users):
		return user["groups"]

def get_default_user_groups():
	r_t = get("group")
	return r_t["defaults"]

def get_all_users():
	r_t = get("user")
	return r_t

def get_normal_users():
	users = get("user")
	nusers = []
	for user in users:
		if int(user["uid"]) >= 1000:
			nusers.append(user)
	return nusers 

def change_user_pass(username,new_pass,auth_user, auth_pass):
	data = {
		"user" : username,
		"passwd" : new_pass,
		"auth_user" : auth_user,
		"auth_passwd" : auth_pass
	}
	r_t =  post("user/passwd", data)
	return r_t

# gereksiz ise silinecek
def pass_to_hash(passwd,salt=False):
	get = subprocess.Popen(["sh","-c",'echo "{}" | openssl passwd -6 -stdin'.format(passwd)], stdout = subprocess.PIPE)
	get.wait()
	get = get.communicate()[0]
	get = get.decode("utf-8","strict")
	return get[:-1]

def add_user(username,desc,home,shell,groups,auth_user,auth_pass):
	data = {
		"user"   : username,
		"desc"   : desc,
		"groups" : groups,
		"auth_user" : auth_user,
		"auth_passwd" : auth_pass,
	}
	if shell :
		data["shell"] = "true"
	if home :
		data["home"] = "true"	
	
	r_t =  post("user", data)
	print(r_t)
	return r_t

def get_timezone():
	data = get("init")
	return data["clock"]["timezone"]
	#return run_process("datetime/timezone").replace("\n","")

def set_timezone(zone):
	r_t = run_process(f'datetime/tzset?zone={zone}')

def set_hostname(h_name, passwd):
	return run_process(f'system/_hostname?name={h_name}&_pwd_={passwd}')

def get_datetime_utc():
	data = get("init")
	return data["clock"]["utc"]
	#return run_process("datetime/utc")

def set_datetime_utc(status):
	r_t = run_process(f'datetime/utcset?status={status}')

def set_datetime_manuel(time):
	time = time.replace(" ","%20")
	r_t = run_process(f'datetime/manual?time={time}')

def activate_ntp():
	return run_process('datetime/ntpon')

def deactivate_ntp():
	return run_process('datetime/ntpoff')

def test_ayguci():
	import socket
	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.settimeout(1)                                      #2 Second Timeout
	result = sock.connect_ex(('127.0.0.1',ayguci_port))
	if result == 0:
	  return True
	else:
	  return False
