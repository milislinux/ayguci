package main

import (
  "github.com/labstack/echo/v4"
  "github.com/labstack/echo/v4/middleware"
  "net/http"
  "html/template"
  "io"
  "fmt"
  "time"
  "log"
  "os"
  "os/user"
  "os/exec"
  "strconv"
  "strings"
  //"github.com/imroc/req/v3"
  "gopkg.in/ini.v1"
  //"crypto/subtle"
  "encoding/json"
  //"github.com/zcalusic/sysinfo"
  //"github.com/lawl/pulseaudio"
  "github.com/msteinert/pam"
  "errors"
  "flag"
  "path"
  "path/filepath"
  "math"
  "slices"
  "bufio"
)

//var token string
//var API string
//var headers map[string]string

type CmdResp struct {
    Section string `json:"section"`
    Key string	   `json:"key"`
    Out string     `json:"out"`
    Err string     `json:"err"`
}

type Credentials struct {
	User     string
	Password string
}

var cfg *ini.File
var user_id string
var duser *user.User

var cfg_file *string

var funcMap = template.FuncMap{
    "subTime": sub_time,
}

type Template struct {
    templates *template.Template
}

func (c Credentials) RespondPAM(s pam.Style, msg string) (string, error) {
	switch s {
	case pam.PromptEchoOn:
		return c.User, nil
	case pam.PromptEchoOff:
		return c.Password, nil
	}
	return "", errors.New("unexpected")
}


func (t *Template) Render(w io.Writer, name string, data interface{}, c echo.Context) error {
    return t.templates.ExecuteTemplate(w, name, data)
}

func sub_time(a, b time.Time) time.Duration {
    return b.Sub(a)
}

func check_command(command string) error {
	_, err := exec.LookPath(command)
	return err
}

func auth_check(password string) error {
	if get_user_id() != "-1" {
	  duser,_ = user.LookupId(user_id)
	  c := Credentials{
		User:     duser.Username,
		Password: password,
	  }
	  tx, err := pam.Start("system-auth", "", c)
	  if err != nil {
		fmt.Fprintf(os.Stderr, "start: %s\n", err.Error())
	  }
	  return tx.Authenticate(0)
	} else {
	  return errors.New("non-desktop session") 
    }
}

// masaüstünü açan aktif kullanıcıyı al
func get_user_id() string {
    cmd := "file /tmp/runtime-*"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    if strings.Contains(string(out), ": directory") {
      user_id_temp := strings.Split(string(out), ":")[0]
      user_id = strings.Split(user_id_temp, "-")[1]
      return user_id
	} else {
      return "-1"
    }
}

// todo!! root ve wheel şeklinde olacak
func wheel_check(user string) error {
	err := errors.New("unauthorized")
	for _, u := range user_json() {
	  if u["username"] == user && slices.Contains(u["groups"].([]string), "wheel")  {
		  return nil
	  }
	}
	return err
}

func load_cfg() {
  var err error
  cfg, err = ini.LoadSources(ini.LoadOptions{IgnoreInlineComment: true,},*cfg_file)
  if err != nil {
    fmt.Printf("Fail to read file: %v", err)
    os.Exit(1)
  }	
}

// ini nesne alıp string-interface dönüştürür
// json aktarımları için
func ini_map(inio *ini.File) map[string]interface{} {
	i_map := make(map[string]interface{})
	for _,sect := range inio.Sections() {
	  el := make(map[string]string)
	  for _, ky := range sect.Keys() {
	    el[ky.Name()] = ky.String()
	  }
	  i_map[sect.Name()] = el
	}
	return i_map
}

func main() {
  // PATH tanımlama
  os.Setenv("PATH","/bin:/sbin:/usr/bin:/usr/sbin:/usr/milis/bin:/usr/milis/mps/bin:/usr/local/bin")
  // xdg_runtime dizin 
  ////os.Setenv("XDG_RUNTIME_DIR","/tmp/runtime-"+user_id)
  	
  ayguci_dir := path.Dir(os.Args[0])
  cfg_file = flag.String("c", ayguci_dir + "/cmd.ini", "config file")
  flag.Parse()
  
  current, err := user.Current()
  if err != nil {
	 log.Fatal(err)
  }

  if current.Uid != "0" {
	log.Fatal("requires superuser privilege")
  }
  // ayguci.ini ayarları yükle
  load_cfg()
    
  //token = cfg.Section("api").Key("token").String()
  //API   = cfg.Section("api").Key("url").String()
  ////user_id = "1000" //strings.Split(os.Getenv("XDG_RUNTIME_DIR"),"-")[1]
  // sudo ile başlarsa SUDO_USER e direk bakılabilir
  ////fmt.Println("user-id:",user_id) 	 
  ////duser,_ = user.LookupId(user_id)
  ////fmt.Println("user-name:",duser.Username)
  t := &Template{
    //templates: template.Must(template.ParseGlob("templates/*.html")),
    templates: template.Must(template.New("").Funcs(funcMap).ParseGlob(ayguci_dir + "/templates/*.html")),
  }
  
  // Echo instance
  e := echo.New()
  
  // static files
  e.Static("/static", ayguci_dir + "/static")
  
  // Middleware
  e.Use(middleware.Logger())
  e.Use(middleware.Recover())
  
  // templates
  e.Renderer = t
	
  // Routes
  e.GET("/", index)
  e.POST("/", index)
  
  e.GET("/api/os", os_api)
  e.GET("/api/cpu", cpu_api)
  e.GET("/api/memory", memory_api)
  
  e.GET("/api/user", user_api)
  e.POST("/api/user", user_api)
  e.POST("/api/user/passwd", user_passwd_api)
  e.DELETE("/api/user", user_api)
  
  e.GET("/api/group", group_api)
  e.GET("/user", user_page)
  
  e.GET("/api/process", process_api)
  e.GET("/process", process_page)
  
  e.GET("/api/port", port_api)
  e.GET("/port", port_page)
  
  e.GET("/api/usb", usb_api)
  e.GET("/usb", usb_page)
  
  e.GET("/api/disk", disk_api)
  e.GET("/disk", disk_page)
  
  e.GET("/api/sound", sound_api)
  e.GET("/sound", sound_page)
  
  e.GET("/api/sensor", sensor_api)
  e.GET("/sensor", sensor_page)
  
  e.GET("/api/init", init_api)
  e.GET("/init", init_page)
  
  e.GET("/api/kernel", kernel_api)
  e.GET("/kernel", kernel_page)
  
  e.GET("/api/hardware", hardware_api)
  e.GET("/hardware", hardware_page)
    
  e.GET("/api/battery", bat_api)
  e.GET("/battery", bat_page)
  
  e.GET("/api/locale", locale_api)
  e.POST("/api/locale", locale_api)
  e.GET("/locale", locale_page)
    
  e.GET("/api/mps", mps_api)
  e.GET("/api/mps/config", mps_config_api)
  e.GET("/api/mps/gun", mps_gun_api)
  e.GET("/mps/talimat", mps_talimat)
  e.GET("/mps", mps_page)
  
  e.GET("/api/boot", boot_api)
  e.GET("/boot", boot_page)
  
  e.GET("/api/network", network_api)
  e.GET("/api/network/active", network_active_api)
  e.GET("/api/network/rfkill", network_rfkill_api)
  e.GET("/network", network_page)
  
  e.GET("/api/service", service_api)
  e.GET("/service", service_page)
  
  e.GET("/api/screen", screen_api)
  e.GET("/api/screen/card", screen_hw_api)
  e.GET("/screen", screen_page)
  
  e.GET("/api/desktop", desktop_api)
  e.POST("/api/desktop", desktop_api)
  e.GET("/api/desktop/wm", wm_api)
  e.GET("/desktop", desktop_page)
  
  e.GET("/api/python", python_api)
  e.GET("/python", python_page)
  
  e.POST("/api/notify", notify_api)
  e.GET("/api/notify", notify_api)
  e.DELETE("/api/notify", notify_api)
  e.GET("/notify", notify_page)
  
  e.GET("/api/cmd/:sec/:key", cmd_run)
  // Start server
  e.Logger.Fatal(e.Start(":5005"))
}

func request_error(c echo.Context, msg string) error {
	return c.JSON(http.StatusOK, map[string]interface{}{
	  "error": msg,
	})
}

func user_json() []map[string]interface{}{
	cmd := "cat /etc/passwd | jc -r --passwd"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    groups := group_json()
    for _, user := range jout {
	  var ug []string
	  for _, group := range groups {
	    if slices.Contains(group["members"].([]interface{}), user["username"]) {
		  ug = append(ug, group["group_name"].(string))
		}
	  }
	  user["groups"] = ug 
	}
    return jout
}

func group_json() []map[string]interface{}{
	cmd := "cat /etc/group | jc -r --group"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

// Handler
func user_page(c echo.Context) error {
    jout := user_json()
    joutg := group_json()
	return c.Render(http.StatusOK,"user.html",map[string]interface{}{
		"user_menu" : "active",
		"users" : jout,
		"groups" : joutg,
    })
}  

func default_user_groups() []string {
	// todo!! bir ayar dosyasından alınacak
	// kullanıcı oluştururken yetkili veya normal kullanıcı grupları
	return []string{"tty","floppy","disk","lp","audio","video","cdrom","adm","network","input","wheel","users"}
}

func group_api(c echo.Context) error {
	
	jout :=map[string]interface{}{
        "groups": group_json(),
        "defaults": default_user_groups(),
    }
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data" :  jout,
	})
} 

func user_api(c echo.Context) error {
	method := c.Request().Method
	
	if method == "GET" {
	  jout := user_json()
	  return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	  })
	}
	if method == "POST" {
	  user_post := make(map[string]interface{})
	  err := json.NewDecoder(c.Request().Body).Decode(&user_post)
	  if err != nil {
	    return request_error(c, "could not decoded")
	  }
	  // check json
	  for _, jk := range([]string{"user","groups","auth_user","auth_passwd"}) {
	    if user_post[jk] == nil {
	      return request_error(c, fmt.Sprintf("missing %s", jk))
	    } 
	    if user_post[jk] == "" {
	      return request_error(c, "empty "+jk)
	    }
	  }
	  // ini içinde section kontrolü
	  mode := "useradd"
	  users := user_json()
	  user := user_post["user"].(string)
	  // mode
	  for _, u := range users {
	    if u["username"] == user {
		  mode = "usermod"
		}
	  }
	  // groups
	  // todo!! grupsuz olabilir - kontrol edilecek
	  p_groups := user_post["groups"].(string)
	  groups := ""
	  // check default groups request
	  // todo!! check "default" is not group
	  if p_groups == "default" {
		groups = strings.Join(default_user_groups(),",")
	  } else {
	    for _, pg := range strings.Fields(p_groups) {
	      not_found := true
	      for _, g := range group_json() {
		    if g["group_name"] == pg {
		      not_found = false
		    }
		  }
		  if not_found {
		    return request_error(c, "invalid group "+pg)
		  }
	    }
	    groups = strings.Join(strings.Fields(p_groups),",")
      }
	  
	  // homedir
	  home_dir := ""
	  if user_post["home"] != nil {
		if user_post["home"] == "true" || user_post["home"] == "" {
	      user_post["home"] = fmt.Sprintf("/home/%s",user)
	    } 
	    home_dir = fmt.Sprintf(" -m -d %s ",user_post["home"])
	  }
	  // desc
	  if user_post["desc"] == nil {
	    user_post["desc"] = user
	  }
	  // shell
	  if user_post["shell"] == nil {
	      user_post["shell"] = "/bin/false"
	  } else {
	    if user_post["shell"] == "true" || user_post["shell"] == "" {
	      user_post["shell"] = "/bin/bash"
	    } 
	  }
	  user_post["mode"] = mode 
	  user_post["groups"] = groups
	  auth_user := user_post["auth_user"].(string)
	  auth_passwd := user_post["auth_passwd"].(string)
	  // yetki kontrol
	  if auth_check(auth_passwd) == nil && wheel_check(auth_user) == nil {
	    cmd := fmt.Sprintf(`%s -c '%s' %s -G '%s' -s %s %s`,user_post["mode"], 
	    user_post["desc"], home_dir, user_post["groups"], user_post["shell"],
	    user_post["user"])
	    user_post["cmd"] = cmd
		exec.Command("sh", "-c",cmd).Output()
		user_post[mode] = "ok"
		user_post["cmd"] = cmd
	  } else {
	    user_post["error"] = "unauthorized"
	  } 
	  return c.JSON(http.StatusOK, map[string]interface{}{
		"data": user_post,
	  })
	}
	if method == "DELETE" {
	  user_post := make(map[string]interface{})
	  err := json.NewDecoder(c.Request().Body).Decode(&user_post)
	  if err != nil {
	    return request_error(c, "could not decoded")
	  }
	  // check json
	  for _, jk := range([]string{"user","auth_user","auth_passwd"}) {
	    if user_post[jk] == nil {
	      return request_error(c, fmt.Sprintf("missing %s", jk))
	    } 
	    if user_post[jk] == "" {
	      return request_error(c, "empty "+jk)
	    }
	  }
	  user_post["mode"] = "userdel"
	  users := user_json()
	  user := user_post["user"].(string)
	  auth_user := user_post["auth_user"].(string)
	  auth_passwd := user_post["auth_passwd"].(string)
	  // mode
	  not_found := true
	  // check user exist
	  for _, u := range users {
	    if u["username"] == user {
		  not_found = false
		}
	  }
	  if not_found {
	    return request_error(c, "invalid "+user)
	  }
	  // check self deleting
	  if user == auth_user {
	    return request_error(c, "self-deletion is not allowed!")
	  }
	  // yetki kontrol
	  if auth_check(auth_passwd) == nil && wheel_check(auth_user) == nil {
	    // default force
	    options := " -f -r "
	    cmd := fmt.Sprintf(`%s %s %s`,user_post["mode"] , options, user )
		exec.Command("sh", "-c",cmd).Output()
		user_post["delete"] = "ok"
		user_post["cmd"] = cmd
	  } else {
	    user_post["error"] = "unauthorized"
	  }
	  return c.JSON(http.StatusOK, map[string]interface{}{
		"data": user_post,
	  })
	}
	return nil
}  

func user_passwd_api(c echo.Context) error {
	method := c.Request().Method

	if method == "POST" {
	  user_post := make(map[string]interface{})
	  err := json.NewDecoder(c.Request().Body).Decode(&user_post)
	  if err != nil {
	    return request_error(c, "could not decoded")
	  }
	  // check json
	  for _, jk := range([]string{"user","passwd","auth_user","auth_passwd"}) {
	    if user_post[jk] == nil {
	      return request_error(c, fmt.Sprintf("missing %s", jk))
	    } 
	    if user_post[jk] == "" {
	      return request_error(c, "empty "+jk)
	    }
	  }
	  // check user
	  not_found := true
	  for _, u := range user_json() {
	    if u["username"] == user_post["user"].(string)  {
		  not_found = false
	    }
	  }
	  if not_found {
	    return request_error(c, "invalid user: " + user_post["user"].(string))
	  }
	  if auth_check(user_post["auth_passwd"].(string)) == nil && wheel_check(user_post["auth_user"].(string)) == nil {
	    cmd := "echo '%s:%s' | chpasswd -c SHA512"
		exec.Command("sh", "-c",fmt.Sprintf(cmd,user_post["user"],user_post["passwd"])).Output()
		user_post["change"] = "ok" // or error
	  } else {
	    user_post["error"] = "unauthorized"
	  }
	  // 
	  return c.JSON(http.StatusOK, map[string]interface{}{
		"data": user_post,
	  })
	}
	return nil
} 

func process_json() []map[string]interface{}{
	cmd := "ps aux | jc -r --ps"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

// Handler
func process_page(c echo.Context) error {
    jout := process_json()
	return c.Render(http.StatusOK,"process.html",map[string]interface{}{
		"process_menu" : "active",
		"data" : jout,
    })
}  

func process_api(c echo.Context) error {
	jout := process_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
} 

//sound
func sound_json() []map[string]interface{}{
	var jout []map[string]interface{}
	if get_user_id() != "-1" {
	  cmd := "pactl -f json -s /tmp/runtime-"+user_id+"/pulse/native list sinks"	
      out, _ := exec.Command("sh", "-c",cmd).Output()
      json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
	} 
    return jout
}

// Handler
func sound_page(c echo.Context) error {
    //jout := sound_json()
	return c.Render(http.StatusOK,"sound.html",map[string]interface{}{
		"sound_menu" : "active",
		//"data" : jout,
    })
}  

func sound_api(c echo.Context) error {
	jout := sound_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}


func port_json() []map[string]interface{}{
	cmd := "netstat -natplu | jc -p --netstat"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

// Handler
func port_page(c echo.Context) error {
    jout := port_json()
	return c.Render(http.StatusOK,"port.html",map[string]interface{}{
		"port_menu" : "active",
		"data" : jout,
    })
}  

func port_api(c echo.Context) error {
	jout := port_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func usb_json() []map[string]interface{}{
	cmd := "lsusb | jc -r --lsusb"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    for _,obj := range(jout) {
		obj["id2"] = strings.Replace(obj["id"].(string),":","-",-1)
	}
    return jout
}

// Handler
func usb_page(c echo.Context) error {
    jout := usb_json()
	return c.Render(http.StatusOK,"usb.html",map[string]interface{}{
		"usb_menu" : "active",
		"data" : jout,
    })
}  

func usb_api(c echo.Context) error {
	jout := usb_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
} 

// disk
func disk_json() map[string]interface{}{
	//blkid
	/*
	cmd := "blkid | jc -r --blkid"
    out, _ := exec.Command("sh", "-c",cmd).Output()  
    var blkid []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &blkid)
	*/
	// lsblk -o NAME,FSTYPE,LABEL,PARTLABEL,UUID,FSAVAIL,FSUSE%,MOUNTPOINTS
	//lsblk
	cmd2 := "lsblk -o NAME,FSTYPE,LABEL,PARTLABEL,UUID,FSSIZE,FSUSED,FSAVAIL,FSUSE%,MOUNTPOINT | jc -r --lsblk"
    out2, _ := exec.Command("sh", "-c",cmd2).Output()  
    var lsblk []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out2),"\n","",-1)), &lsblk)
    //mount
	cmd3 := "mount | jc -r --mount"
    out3, _ := exec.Command("sh", "-c",cmd3).Output()  
    var mount []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out3),"\n","",-1)), &mount)
    //fstab
	cmd4 := "cat /etc/fstab | jc --fstab"
    out4, _ := exec.Command("sh", "-c",cmd4).Output()  
    var fstab []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out4),"\n","",-1)), &fstab)
	jout :=map[string]interface{}{
        //"blkid": blkid ,
        "lsblk": lsblk ,
        "mount": mount ,
        "fstab": fstab ,
    }
    return jout
}

// Handler
func disk_page(c echo.Context) error {
    jout := disk_json()
	return c.Render(http.StatusOK,"disk.html",map[string]interface{}{
		"disk_menu" : "active",
		"data" : jout,
    })
}  

func disk_api(c echo.Context) error {
	jout := disk_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func sensor_json() map[string]interface{}{
	cmd := "sensors -j"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

// Handler
func sensor_page(c echo.Context) error {
    //jout := sensor_json()
	return c.Render(http.StatusOK,"sensor.html",map[string]interface{}{
		"sensor_menu" : "active",
		//"data" : jout,
    })
}  

func sensor_api(c echo.Context) error {
	jout := sensor_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
} 

func init_json() map[string]interface{}{
	cmd := "cat /etc/init/system.ini | jc --ini"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

// Handler
func init_page(c echo.Context) error {
    //jout := init_json()
	return c.Render(http.StatusOK,"init.html",map[string]interface{}{
		"init_menu" : "active",
		//"data" : jout,
    })
}  

func init_api(c echo.Context) error {
	jout := init_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func hardware_json() map[string]interface{}{
	cmd := "lshw -json"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

// Handler
func hardware_page(c echo.Context) error {
	if check_command("lshw") != nil {
		return c.HTML(http.StatusOK,"gerekli komut: lshw")
	}
    //jout := hardware_json()
	return c.Render(http.StatusOK,"hardware.html",map[string]interface{}{
		"hardware_menu" : "active",
		//"data" : jout,
    })
}  

func hardware_api(c echo.Context) error {
	jout := hardware_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

// eski desktop_json
func _desktop_json() map[string]interface{}{
	var jout map[string]interface{}
	if get_user_id() != "-1" {
	  duser,_ = user.LookupId(user_id)
	  cmd := "cat /home/"+duser.Username+"/.config/masa.ini | jc --ini"
      out, _ := exec.Command("sh", "-c",cmd).Output()
      json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
	}
    return jout
}

func wm_json() map[string]interface{}{
    jout := map[string]interface{}{
        "active": "" ,
        "env": "" ,
    }
    if get_user_id() != "-1" {
      cmd := fmt.Sprintf(`lsof -Ua /tmp/runtime-%s/wayland-* | grep LISTEN | awk 'NR==1{print "{\"pid\":\"" $2 "\",\"name\":\"" $1 "\"}" }'`,user_id)
      out, _ := exec.Command("sh", "-c",cmd).Output()
      var wm map[string]interface{}
      json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &wm)
    
      cmd2 := `cat /proc/`+wm["pid"].(string)+`/environ | tr '\0' '\n' | jc --env`
      out2, _ := exec.Command("sh", "-c",cmd2).Output()
      var envi []map[string]interface{}
   
      envistr := strings.Split(string(out2),"\\nBASH_FUNC_")[0] + "\"}]"
      json.Unmarshal([]byte(envistr), &envi)
      //jout :=map[string]interface{}{
      //  "active": wm ,
      //  "env": envi ,
      //}
      jout["active"] = wm
      jout["env"] = envi
	}
    return jout
}

// Handler
func desktop_page(c echo.Context) error {
    jout := wm_json()
	return c.Render(http.StatusOK,"desktop.html",map[string]interface{}{
		"desktop_menu" : "active",
		"data" : jout,
    })
}

func desktop_api(c echo.Context) error {
	if get_user_id() != "-1" {
	  duser,_ = user.LookupId(user_id)
	  method := c.Request().Method
	  // read masa.ini
	  desk_ini := fmt.Sprintf("/home/%s/.config/masa.ini", duser.Username)
	  cfg_desk, err := ini.LoadSources(ini.LoadOptions{IgnoreInlineComment: true,},desk_ini)
      if err != nil {
        return request_error(c, fmt.Sprintf("Fail to read file: %v", err))
      }	
	  if method == "GET" { 
	    desk_map := ini_map(cfg_desk)
	    // gereksiz default 
	    delete(desk_map, "DEFAULT")
	    return c.JSON(http.StatusOK, map[string]interface{}{
		  "data": desk_map,
	    })
	  }
	  if method == "POST" {
	    desk_post := make(map[string]interface{})
	    err := json.NewDecoder(c.Request().Body).Decode(&desk_post)
	    if err != nil {
	      return request_error(c, "could not decoded")
	    }
	    // check json
	    for _, jk := range([]string{"section","key","value"}) {
	      if desk_post[jk] == nil {
	        return request_error(c, fmt.Sprintf("missing %s", jk))
	      }
	    }
	    // ini içinde section kontrolü
	    section := desk_post["section"].(string)
	    key := desk_post["key"].(string)
	    value := desk_post["value"].(string)
	    if ! cfg_desk.HasSection(section) {
	      return request_error(c, "invalid section")
	    }
	    if key == "" {
	      return request_error(c, "empty key")
	    }
	    // key silme - value boş gönderilir 
	    if value == "" {
	      cfg_desk.Section(section).DeleteKey(key)
	    } else {
		  cfg_desk.Section(section).Key(key).SetValue(value)
	    }
	    // kayıt edildiğine dair status ekle
	    // dosya kaydet
	    cfg_desk.SaveTo(desk_ini)
	    return c.JSON(http.StatusOK, map[string]interface{}{
		  "data": ini_map(cfg_desk)[section],
	    })
	  }
	}
	return nil
}

func wm_api(c echo.Context) error {
	jout := wm_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func kernel_json() []map[string]interface{}{
	cmd := "lsmod | jc --lsmod"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

// Handler
func kernel_page(c echo.Context) error {
    jout := kernel_json()
	return c.Render(http.StatusOK,"kernel.html",map[string]interface{}{
		"kernel_menu" : "active",
		"data" : jout,
    })
}  

func kernel_api(c echo.Context) error {
	jout := kernel_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
} 

//screen
func screen_json() []map[string]interface{}{
	var jout []map[string]interface{}
	if get_user_id() != "-1" {
	  os.Setenv("XDG_RUNTIME_DIR","/tmp/runtime-"+user_id)
	  cmd := "wlr-randr --json"
      out, _ := exec.Command("sh", "-c",cmd).Output()
      json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    }
    return jout
}

// Handler
func screen_page(c echo.Context) error {
    //jout := screen_json()
	return c.Render(http.StatusOK,"screen.html",map[string]interface{}{
		"screen_menu" : "active",
		//"data" : jout,
    })
}  

func screen_api(c echo.Context) error {
	jout := screen_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func screen_hw_json() []map[string]interface{}{
	cmd := "lshw -json -c video"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

func screen_hw_api(c echo.Context) error {
	jout := screen_hw_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

// mps
func mps_gun_json() map[string]interface{}{
	cmd := "mps-update-check.sh"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

func mps_json() []map[string]string{
	var jout []map[string]string
	err := filepath.Walk("/usr/milis/talimatname",
	func(path string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}
	if filepath.Base(path) == "talimat" {
		tdir := filepath.Base(filepath.Dir(path))
		seviye := strings.Split(strings.Split(path,"/usr/milis/talimatname/")[1],"/")[0]
		isim := strings.Split(tdir, "#")[0]
		surum := strings.Split(strings.Split(tdir, "#")[1], "-")[0]
		devir := strings.Split(strings.Split(tdir, "#")[1], "-")[1]
		cfg, _ := ini.Load(path)
		paket := map[string]string{
                  "name" : isim,
                  "version" : surum,
                  "release" : devir,
                  "desc" : cfg.Section("paket").Key("tanim").String(),
                  "group" : cfg.Section("paket").Key("grup").String(),
                  "url" : cfg.Section("paket").Key("url").String(),
                  "path" : path,
                  "level" : seviye,
		}
		jout = append(jout, paket)
	}
	return nil
	})
	if err != nil {
		log.Println(err)
	}
    
    //json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

func mps_page(c echo.Context) error {
    jout := mps_json()
	return c.Render(http.StatusOK,"mps.html",map[string]interface{}{
		"mps_menu" : "active",
		"data" : jout,
    })
}  

func mps_talimat(c echo.Context) error {
    path := c.QueryParam("talimat")
    if strings.Contains(path, "/usr/milis/talimatname") {
		return c.Inline(path, "talimat")
	} else {
		return nil
	}
} 

func mps_api(c echo.Context) error {
	jout := mps_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
} 

func mps_config_api(c echo.Context) error {
	jout := mps_config_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func mps_gun_api(c echo.Context) error {
	jout := mps_gun_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func mps_config_json() map[string]interface{}{
	cmd := "cat /usr/milis/mps/conf/mps.ini | jc --ini"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

func bat_json() map[string]interface{}{
	cmd := "acpi -bia | jc --acpi"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var bat []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &bat)
    
    cmd2 := "lshw -json -c power"
    out2, _ := exec.Command("sh", "-c",cmd2).Output()
    var hw []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out2),"\n","",-1)), &hw)
    
    jout := map[string]interface{}{
		"state" : bat[0],
		"adapter" : bat[1],
		"hardware" : hw,
	}
    return jout
}

// Handler
func bat_page(c echo.Context) error {
    for _,req := range []string{"lshw","acpi","jc"} {
		if check_command(req) != nil {
			return c.HTML(http.StatusOK,"gerekli komut: "+req)
		}
	}
    //jout := bat_json()
	return c.Render(http.StatusOK,"battery.html",map[string]interface{}{
		"battery_menu" : "active",
		//"data" : jout,
    })
}  

func bat_api(c echo.Context) error {
	jout := bat_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

//locale
func locale_json() map[string]interface{}{
    cmd := "locale -a | jq  --raw-input .  | jq --slurp ."
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var supported []string
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &supported)

    cmd2 := "locale | jc --ini"
    out2, _ := exec.Command("sh", "-c",cmd2).Output()
    var active map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out2),"\n","",-1)), &active)

    cmd3 := "date | jc --date -p"
    out3, _ := exec.Command("sh", "-c",cmd3).Output()
    var datetime map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out3),"\n","",-1)), &datetime)

    cmd4 := "file /etc/localtime | rev |cut -d/ -f1 | rev"
    out4, _ := exec.Command("sh", "-c",cmd4).Output()
    datetime["timezone"] = strings.Replace(string(out4),"\n","",-1)

    cmd5 := "date +%Z"
    out5, _ := exec.Command("sh", "-c",cmd5).Output()
    datetime["utc_offset"] = strings.Replace(string(out5),"\n","",-1)

    delete(datetime, "timezone_aware")
    delete(datetime, "epoch_utc")

    cmd6 := "cat /usr/share/i18n/SUPPORTED | jq  --raw-input .  | jq --slurp ."
    out6, _ := exec.Command("sh", "-c",cmd6).Output()
    var supports []string
    json.Unmarshal([]byte(strings.Replace(string(out6),"\n","",-1)), &supports)

    jout := map[string]interface{}{
		"supported" : supported,
		"active" : active,
		"date" : datetime,
		"supports": supports,
	}
    return jout
}

// Handler
func locale_page(c echo.Context) error {
    //jout := locale_json()
	return c.Render(http.StatusOK,"locale.html",map[string]interface{}{
		"locale_menu" : "active",
		//"data" : jout,
    })
}  

func locale_api(c echo.Context) error {
	method := c.Request().Method
	if method == "GET" {
	  jout := locale_json()
	  return c.JSON(http.StatusOK, map[string]interface{}{
	    "data": jout,
	  })
	}
	if method == "POST" {
	  locale_post := make(map[string]interface{})
	  err := json.NewDecoder(c.Request().Body).Decode(&locale_post)
	  if err != nil {
	    return request_error(c, "could not decoded")
	  }
	  //fmt.Println("postbody:",locale_post)
	  oper := ""
	  locale := ""
	  
	  if locale_post["add"] != nil {
	    oper = "add"
	  }
	  if locale_post["del"] != nil {
	    oper = "del"
	  }
	  if oper == "" {
	    return request_error(c, "invalid operation")
	  } else {
	    locale = locale_post[oper].(string)
	  }
	  
	  cmd := `locale.gen.sh %s "%s"`
	  cmd = fmt.Sprintf(cmd, oper, locale)
      // todo!!! tasker ile yapılacak
      go exec.Command("sh", "-c",cmd).Output()
	  locale_post["status"] = "ok"
	  return c.JSON(http.StatusOK, map[string]interface{}{
		"data": locale_post,
	  })
	}
	return nil
}

func boot_json() map[string]interface{}{
	cmd := `cat /boot/limine.cfg | sed s#^:#[:#g | sed 's#\[.*#&]#g' | jc --ini`
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

// Handler
func boot_page(c echo.Context) error {
    for _,req := range []string{"limine","jc"} {
		if check_command(req) != nil {
			return c.HTML(http.StatusOK,"gerekli komut: "+req)
		}
	}
    //jout := boot_json()
	return c.Render(http.StatusOK,"boot.html",map[string]interface{}{
		"boot_menu" : "active",
		//"data" : jout,
    })
}  

func boot_api(c echo.Context) error {
	jout := boot_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func network_active_json() map[string]interface{}{
	cmd := "connmanctl services `connmanctl services | grep \"*A\" | awk '{print $3}'` | tail -n +2 | jc --ini"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}


func network_rfkill_json() []map[string]interface{}{
	cmd := "rfkill -J | jq .rfkilldevices"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

func network_json() []map[string]interface{}{
	cmd := "ip -j a | jq -r"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

// Handler
func network_page(c echo.Context) error {
    jout := network_json()
    rfkill := network_rfkill_json()
	return c.Render(http.StatusOK,"network.html",map[string]interface{}{
		"network_menu" : "active",
		"data" : jout,
		"rfkill" : rfkill,
    })
}  

func network_api(c echo.Context) error {
	jout := network_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func network_active_api(c echo.Context) error {
	jout := network_active_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func network_rfkill_api(c echo.Context) error {
	jout := network_rfkill_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

// python
func python_json() map[string]interface{}{
	if get_user_id() != "-1" {
	  duser,_ = user.LookupId(user_id)
	  cmd := "pip3 list | jc --pip-list"
      out, _ := exec.Command("sh", "-c",cmd).Output()
      var system []map[string]interface{}
      json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &system)
      cmd2 := fmt.Sprintf("su - %s -c 'pip3 list --user' | jc --pip-list",duser.Username)
      out2, _ := exec.Command("sh", "-c",cmd2).Output()
      var user []map[string]interface{}
      json.Unmarshal([]byte(strings.Replace(string(out2),"\n","",-1)), &user)
      jout := map[string]interface{}{
	    "system" : system,
		"user"   : user,
	  }
	  return jout
    }
    return nil
}

// Handler
func python_page(c echo.Context) error {
    jout := python_json()
	return c.Render(http.StatusOK,"python.html",map[string]interface{}{
		"python_menu" : "active",
		"data" : jout,
    })
}  

func python_api(c echo.Context) error {
	jout := python_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func notify_json() []map[string]interface{}{
	notify_log := "/var/log/ayguci_notifications.json"
	file, _ := os.OpenFile(notify_log, os.O_RDONLY, os.ModePerm)
    defer file.Close()  
    scanner := bufio.NewScanner(file)
    var jout []map[string]interface{}
    for scanner.Scan() {
      jin := make(map[string]interface{})
      json.Unmarshal([]byte(scanner.Text()), &jin)
      jout = append(jout, jin)
    }
    return jout
}

func notify_page(c echo.Context) error {
    jout := notify_json()
	return c.Render(http.StatusOK,"notify.html",map[string]interface{}{
	  "notify_menu" : "active",
	  "data" : jout,
    })
}  

func notify_api(c echo.Context) error {
	if get_user_id() != "-1" {
	  duser,_ = user.LookupId(user_id)
	} else {
	  return errors.New("non-desktop session")
    }
	method := c.Request().Method
	jin := make(map[string]interface{})
	notify_log := "/var/log/ayguci_notifications.json"
	
	if method == "POST" {	  
	  err := json.NewDecoder(c.Request().Body).Decode(&jin)
	  if err != nil {
	    return request_error(c, "could not decoded")
	  }
	  jin["time"] = time.Now().Format("2006.01.02 15:04:05")
	  title := jin["title"]
	  body := jin["body"]
	  mode := jin["mode"]
	  
	  // bildirimi kayıt et
	  file, _ := os.OpenFile(notify_log, os.O_WRONLY|os.O_CREATE|os.O_APPEND, os.ModePerm)
      defer file.Close()  
      jinb, _ := json.Marshal(jin)
      file.Write(jinb)
      file.WriteString("\n")
      
      // bildirimin moduna göre işlem yap ve mode güncelle
	  if mode == "save" {
        jin["mode"] = "saved"
	  } else {
	    cmd_f := "DBUS_SESSION_BUS_ADDRESS=\"unix:path=$(ls /tmp/dbus-*)\" DISPLAY= sudo -E -u %s notify-send '%s' '%s'"
	    exec.Command("sh", "-c",fmt.Sprintf(cmd_f, duser.Username, title, body)).Output()
        jin["mode"] = "notified"
      }
	  return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jin,
	  })
    }
    if method == "DELETE" {
      err := os.Remove(notify_log)  // remove a single file
	  if err != nil {
        fmt.Println(err)
      }
      jin["notifications"] = "deleted"
      return c.JSON(http.StatusOK, map[string]interface{}{
	    "data": jin,
	  })
	}
    // GET isteği
    jout := notify_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
	  "data": jout,
	})
}

func service_json() []map[string]string{
	var err error
	var jout []map[string]string
	
	cmd := "service eliste"
    out, err := exec.Command("sh", "-c",cmd).Output()
    eliste := strings.Split(string(out), "\n")
    cmd = "service liste"
    out, err = exec.Command("sh", "-c",cmd).Output()
    liste := strings.Split(string(out), "\n")
    cmd = "service dliste"
    out, err = exec.Command("sh", "-c",cmd).Output()
    dliste := strings.Split(string(out), "\n")
    fmt.Println("err",err)
    for _,serv := range dliste {
	  if serv == "" {continue}
	  added := "-"
	  status := ""
	  if slices.Contains(eliste, serv) {
		  added = "+"
		  // varsayılan oneshot o durumu eklenir.
		  status = "o"
		  outs, _ := exec.Command("sh", "-c","servis bil "+serv).Output()
		  // servis durumuna göre çalışma durumu eklenir.
		  if strings.Contains(string(outs),"[+]") { status = "+"}
		  if strings.Contains(string(outs),"[X]") { status = "X"}
	  }
	  jout = append(jout, map[string]string {
	    "name" : serv,
	    "local" : added,
	    "active" : strconv.FormatBool(slices.Contains(liste, serv)),
	    "repo" : "+",
	    "status" : status,
	  })
	}
    return jout
}

func service_page(c echo.Context) error {
    jout := service_json()
	return c.Render(http.StatusOK,"service.html",map[string]interface{}{
		"service_menu" : "active",
		"data" : jout,
    })
}

func service_api(c echo.Context) error {
	jout := service_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func os_json() map[string]interface{}{
	// root path atamaları öncesi başladığı için bu tip komutlarda tam yol verilmeli veya path eklemeli
	cmd := "/usr/milis/bin/lsb_release -a | jc --lsb-release"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    out2, _ := exec.Command("sh", "-c","uptime -s").Output()
    out3, _ := exec.Command("sh", "-c","cat /etc/milis-surum").Output()
    var jout map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    jout["uptime"] = strings.TrimSuffix(string(out2),"\n") 
    jout["milis_surum"] = strings.TrimSuffix(string(out3),"\n")
    return jout
}

func uname_json() map[string]interface{}{
	cmd := "uname -a | jc --uname"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

func memory_json() map[string]interface{}{
	cmd := "free -m | jc --free"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    total,_ := jout[0]["total"].(float64)
    used,_ := jout[0]["used"].(float64)
    jout[0]["total"] = math.Floor(total)
    jout[0]["used"] = math.Floor(used)
    jout[0]["free"] = math.Floor(total - used)
    return jout[0]
}

func _memory_json() []map[string]interface{}{
	cmd := "lshw -json -c memory | jq '[ .[] | select(.id | contains(\"bank:\"))]'"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

func cpu_json() map[string]interface{}{
	cmd := "lshw -json -c processor"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout []map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout[0]
}

func hw_json() map[string]interface{}{
	name,_ := os.Hostname()
	cmd := "lshw -json -c system | jq ' .[] | select (.id == \""+name+"\")'"
    out, _ := exec.Command("sh", "-c",cmd).Output()
    var jout map[string]interface{}
    json.Unmarshal([]byte(strings.Replace(string(out),"\n","",-1)), &jout)
    return jout
}

func os_api(c echo.Context) error {
	jout := os_json()
	uname := uname_json()
	jout["kernel_version"] = uname["kernel_version"]
	jout["kernel_release"] = uname["kernel_release"]
	jout["hostname"] = uname["node_name"]
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func cpu_api(c echo.Context) error {
	jout := cpu_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func memory_api(c echo.Context) error {
	jout := memory_json()
	return c.JSON(http.StatusOK, map[string]interface{}{
		"data": jout,
	})
}

func index(c echo.Context) error {
   os := os_json()
   memory := memory_json()
   kernel := uname_json()
   cpu := cpu_json()
   hw := hw_json()
   return c.Render(http.StatusOK, "index.html", map[string]interface{}{
          "index_menu" : "active",
          "os" : os,
          "kernel" : kernel,
          "memory" : memory,
          "cpu"    : cpu, 
          "hw"     : hw, 
   })
}

// api/cmd
func cmd_run(c echo.Context) error {
  load_cfg()
  auth_need := false
  out := []byte("")
  var err string
  var cmd_str string
  sec := c.Param("sec")
  key := c.Param("key")
  
  // key _ ile başlıyorsa yetkili çalıştırma isteyen bir komuttur.
  if string(key[0]) == "_" {
    cmd_str = cfg.Section(sec).Key(key).String()
    auth_need = true
  } else if cfg.Section(sec).Key("_"+key).String() != "" {
	cmd_str = cfg.Section(sec).Key("_"+key).String()
	auth_need = true
  } else {
    cmd_str = cfg.Section(sec).Key(key).String()
  }
  
  cmd_str = cfg.Section(sec).Key(key).String()
  // bu anahtar değeri yoksa yetkili _anahtar ile aranması 
  if cmd_str == "" {
	  cmd_str = cfg.Section(sec).Key("_"+key).String()
	  auth_need = true
  }
 
  fmt.Println(sec,key,cmd_str,auth_need)
  
  if cmd_str != "" {
    // parametre analiz değiştirme
    //cmd_str = "su - "+duser.Username+" -c " + cmd_str
    for k,v := range c.QueryParams() {
      //fmt.Println("k:",k,"v:",v)
      if strings.Contains(cmd_str, "{{"+k+"}}") {
	    cmd_str = strings.Replace(cmd_str, "{{"+k+"}}", v[0], -1)
	  } 
    }
    // auth kontrol - normalde direk kısıtlı yetkili
    if auth_need { 
      if c.QueryParam("_pwd_") != "" {
	    errv := auth_check(c.QueryParam("_pwd_"))
        if errv != nil {
          fmt.Fprintf(os.Stderr, "authenticate: %s\n", errv.Error())
          //out = []byte("authentication failed")
          err = "yetkisiz erişim"
	    } else {
			// auth doğrulandı - auth_need kapat
			auth_need = false
		}
	  } else {
        err = "_pwd_ param missing"
      }
    }
    // auth need kapalı ise
    if !auth_need {
	  // komutlar arası ;; olursa döngünde sırayla çalıştırabilir çıktı kümülatif gönderir.
	  t_out := []byte("_")
	  out = []byte("")
	  var errc error
      for _,cmd := range(strings.Split(cmd_str,";;")) {
		
        //command := strings.Fields(cmd)
        //t_out, err = exec.Command(command[0], command[1:]...).CombinedOutput()
        t_out, errc = exec.Command("sh","-c",cmd).CombinedOutput()
    	out = append(out, t_out...)
        // eğer dönüş string uzunluk kontrol
        if len(out)>0 && string(out[len(out)-1:]) != "\n" {
          out = append(out, []byte("\n")...)
	    } 
      }
      if errc != nil {
        //log.Fatalf("cmd.Run() failed with %s\n", err)
        log.Printf("cmd.Run() failed with %s\n", errc)
      }
    }
  }
  resp := &CmdResp{Section: sec, Key: key, Out: string(out), Err: err}
  respb, _ := json.Marshal(resp)
  fmt.Println("resp:",string(respb))  
  return c.JSONBlob(http.StatusOK, respb)
}

// kaynaklar
// embed templates
// https://github.com/charly3pins/go-embed-example
// https://gist.github.com/tboerger/4840e1b5464fc26fbb165b168be23345
